package main.datastructures.array;

import java.util.ArrayList;
import java.util.List;

/**
 * 数据结构 —— 数组和矩阵
 *
 * @author jingzepei
 * @version 1.0
 * @since 2022/November/7 20:42
 */
public class Application {
    public static void main(String[] args) {
        // 移动0程序运行逻辑
        // int[] nums = new int[]{0, 1, 0, 3, 12};
        // moveZeroes(nums);
        //
        // StringJoiner moveZeroesJoiner = new StringJoiner(",");
        //
        // for (int num : nums) {
        //    moveZeroesJoiner.add(String.valueOf(num));
        //}
        //
        // System.out.println(moveZeroesJoiner);

        // 重塑矩阵
        // int[][] nums = new int[][]{{1, 2}, {3, 4}};
        // int[][] newMat = matrixReshape(nums, 1, 4);
        //
        // for (int[] num : newMat) {
        //     for (int i : num) {
        //         System.out.print(i + "\t");
        //     }
        //
        //     System.out.println();
        // }

        // 最大连续 1 的个数
        // int[] nums = new int[]{1, 1, 0, 1, 1, 1};
        //
        // System.out.println(findMaxConsecutiveOnes(nums));

        // 搜索二维矩阵 II
        // int[][] nums = new int[][]{{1, 4, 7, 11, 15}, {2, 5, 8, 12, 19}, {3, 6, 9, 16, 22}, {10, 13, 14, 17, 24}, {18, 21, 23, 26, 30}};
        //
        // System.out.println(searchMatrix(nums, 5));


        // 错误的集合
        // int[] nums = new int[]{1, 2, 2, 4};
        // int[] newMat = findErrorNums(nums);
        //
        // for (int num : newMat) {
        //     System.out.print(num + "\t");
        // }

        // // 查找重复数
        // int[] nums = new int[]{1, 2, 2, 4};
        // System.out.println(findDuplicate(nums));
    }

    /**
     * <p>题目：移动0</p>
     * <ul>
     *     <li>来源：Leet Code </li>
     *     <li>题号：283</li>
     *     <li>难度: <span style="color:green">简单</span></li>
     *     <li>题目：给定一个数组 nums，编写一个函数将所有 0 移动到数组的末尾，同时保持非零元素的相对顺序。</li>
     *     <li>限制：必须在不复制数组的情况下原地对数组进行操作。</li>
     *     <li>
     *         <p>提示：</p>
     *         <ul>
     *             <li>1 <= nums.length <= 104</li>
     *             <li>-231 <= nums[i] <= 231 - 1</li>
     *         </ul>
     *     </li>
     * </ul>
     *
     * @param nums
     */
    public static void moveZeroes(int[] nums) {
        int index = 0;

        for (int num : nums) {
            if (num != 0) {
                nums[index++] = num;
            }
        }


        while (index < nums.length) {
            nums[index++] = 0;
        }
    }

    /**
     * <p>题目：重塑矩阵</p>
     * <ul>
     *     <li>来源：Leet Code </li>
     *     <li>题号：566</li>
     *     <li>难度: <span style="color:green">简单</span></li>
     *     <li>题目：在 MATLAB 中，有一个非常有用的函数 reshape ，它可以将一个 m x n 矩阵重塑为另一个大小不同（r x c）的新矩阵，但保留其原始数据。
     *              给你一个由二维数组 mat 表示的 m x n 矩阵，以及两个正整数 r 和 c ，分别表示想要的重构的矩阵的行数和列数。
     *              重构后的矩阵需要将原始矩阵的所有元素以相同的 行遍历顺序 填充。
     *              如果具有给定参数的 reshape 操作是可行且合理的，则输出新的重塑矩阵；否则，输出原始矩阵。
     *     </li>
     *     <li>
     *         <p>提示：</p>
     *         <ul>
     *             <li>m == mat.length</li>
     *             <li>n == mat[i].length</li>
     *             <li>1 <= m, n <= 100</li>
     *             <li>-1000 <= mat[i][j] <= 1000</li>
     *             <li>1 <= r, c <= 300</li>
     *         </ul>
     *     </li>
     * </ul>
     *
     * @param mat 二维矩阵
     * @param c   重塑后矩阵列数
     * @param r   重塑后矩阵行数
     */
    public static int[][] matrixReshape(int[][] mat, int r, int c) {
        // 记录原始的长和宽
        int srcRows = mat.length, srcCols = mat[0].length;

        // 排除所有需要返回原始数据的情况
        if (((r * c) != (srcRows * srcCols)) || (r == 0 || c == 0) || (r == srcRows && c == srcCols)) {
            return mat;
        }

        // 将数据整合为一个数组
        int index = 0;
        int[] snap = new int[srcRows * srcCols];
        for (int[] row : mat) {
            for (int item : row) {
                snap[index++] = item;
            }
        }

        // 将一维数组的数分别放入对应的位置
        int[][] res = new int[r][c];
        for (int rowIndex = 0; rowIndex < r; rowIndex++) {
            for (int colIndex = 0; colIndex < c; colIndex++) {
                res[rowIndex][colIndex] = snap[rowIndex * c + colIndex];
            }

        }

        return res;
    }


    /**
     * <p>题目：最大连续 1 的个数</p>
     * <ul>
     *     <li>来源：Leet Code </li>
     *     <li>题号：485</li>
     *     <li>难度: <span style="color:green">简单</span></li>
     *     <li>题目：给定一个二进制数组 nums，计算其中最大连续 1 的个数。</li>
     *     <li>
     *         <p>提示：</p>
     *         <ul>
     *             <li>1 <= nums.length <= 10^5</li>
     *             <li>nums[i]不是0就是1</li>
     *         </ul>
     *     </li>
     * </ul>
     *
     * @param nums 数组
     */
    public static int findMaxConsecutiveOnes(int[] nums) {
        int max = 0, cur = 0;
        for (int i = 0; i < nums.length; i++) {
            if (nums[i] == 1) {
                cur++;
            } else {
                max = Math.max(max, cur);
                cur = 0;
            }
        }

        max = Math.max(max, cur);
        return max;
    }

    /**
     * <p>题目：搜索二维矩阵 II</p>
     * <ul>
     *     <li>来源：Leet Code </li>
     *     <li>题号：240</li>
     *     <li>难度: <span style="color:orange">中等</span></li>
     *     <li>题目：编写一个高效的算法来搜索m x n矩阵 matrix 中的一个目标值 target 。该矩阵具有以下特性：
     *              每行的元素从左到右升序排列。
     *              每列的元素从上到下升序排列。
     *     </li>
     *     <li>
     *         <p>提示：</p>
     *         <ul>
     * 	          <li><code>m == matrix.length</code></li>
     * 	          <li><code>n == matrix[i].length</code></li>
     * 	          <li><code>1 <= n, m <= 300</code></li>
     * 	          <li><code>-10<sup>9</sup>&nbsp;&lt;= matrix[i][j] &lt;= 10<sup>9</sup></code></li>
     * 	          <li>每行的所有元素从左到右升序排列</li>
     * 	          <li>每列的所有元素从上到下升序排列</li>
     * 	          <li><code>-10<sup>9</sup>&nbsp;&lt;= target &lt;= 10<sup>9</sup></code></li>
     *          </ul>
     *     </li>
     * </ul>
     *
     * @param matrix 数组
     * @param target 目标值
     */
    public static boolean searchMatrix(int[][] matrix, int target) {
        if (matrix == null || matrix.length == 0 || matrix[0].length == 0) {
            return false;
        }

        // 从矩形的右上角来看，这个矩形就像是一个二叉树，左子树的值小于根节点，右子树的值大于根节点
        int r = matrix.length, c = matrix[0].length;

        int m = 0, n = c - 1;

        while (m < r && c > -1) {
            if (target == matrix[m][n]) {
                return true;
            } else if (target < matrix[m][n]) {
                n--;
            } else {
                m++;
            }
        }

        return false;
    }


    /**
     * <p>题目：有序矩阵中第 K 小的元素</p>
     * <ul>
     *     <li>来源：Leet Code </li>
     *     <li>题号：378</li>
     *     <li>难度: <span style="color:orange">中等</span></li>
     *     <li>题目：给你一个n x n矩阵matrix ，其中每行和每列元素均按升序排序，找到矩阵中第 k 小的元素。
     *              请注意，它是 排序后 的第 k 小元素，而不是第 k 个 不同 的元素。
     *              你必须找到一个内存复杂度优于O(n2) 的解决方案。
     *     </li>
     *     <li>
     *         <p>提示：</p>
     *         <ul>
     * 	          <li><code>n == matrix.length</code></li>
     * 	          <li><code>n == matrix[i].length</code></li>
     * 	          <li><code>1 <= n <= 300</code></li>
     * 	          <li><code>-10<sup>9</sup>&nbsp;&lt;= matrix[i][j] &lt;= 10<sup>9</sup></code></li>
     * 	          <li>题目数据 保证 matrix 中的所有行和列都按 非递减顺序 排列</li>
     * 	          <li>1 <= k <= n<sup>2</sup></li>
     *          </ul>
     *     </li>
     *     <li>
     *         <p>进阶：</p>
     *         <ul>
     *             <li>你能否用一个恒定的内存(即 O(1) 内存复杂度)来解决这个问题?</li>
     *             <li>
     *                 你能在 O(n) 的时间复杂度下解决这个问题吗?这个方法对于面试来说可能太超前了，但是你会发现阅读这篇文章
     *                  <a href="http://www.cse.yorku.ca/~andy/pubs/X+Y.pdf">this paper</a>
     *                  很有趣。
     *             </li>
     *         </ul>
     *     </li>
     * </ul>
     *
     * @param matrix 数组
     * @param k      第几小元素
     */
    public int kthSmallest(int[][] matrix, int k) {
        int rows = matrix.length, cols = matrix[0].length;

        int low = matrix[0][0], high = matrix[rows - 1][cols - 1];

        // 二分查找法
        while (low <= high) {
            int middle = low + (high - low) / 2;
            int num = 0;

            for (int i = 0; i < rows; i++) {
                for (int j = 0; j < cols && matrix[i][j] <= middle; j++) {
                    num++;
                }
            }

            if (num < k) {
                low = middle + 1;
            } else {
                high = middle - 1;
            }
        }

        return low;
    }

    /**
     * <p>题目：错误的集合</p>
     * <ul>
     *     <li>来源：Leet Code </li>
     *     <li>题号：645</li>
     *     <li>难度: <span style="color:green">简单</span></li>
     *     <li>题目：集合 s 包含从 1 到 n 的整数。不幸的是，因为数据错误，导致集合里面某一个数字复制了成了集合里面的另外一个数字的值，导致集合 丢失了一个数字 并且 有一个数字重复 。
     *              给定一个数组 nums 代表了集合 S 发生错误后的结果。
     *              请你找出重复出现的整数，再找到丢失的整数，将它们以数组的形式返回。
     *     </li>
     *     <li>
     *         <p>提示：</p>
     *         <ul>
     * 	          <li>2 <= nums.length <= 10<sup>4</sup></li>
     * 	          <li>1 <= nums[i] <= 10<sup>4</sup></li>
     *          </ul>
     *     </li>
     * </ul>
     *
     * @param nums 数组
     */
    public static int[] findErrorNums(int[] nums) {
        int[] snap = new int[nums.length + 1];

        for (int i = 0; i < nums.length; i++) {
            snap[nums[i]]++;
        }

        int repeat = 0, defect = 0;

        for (int i = 1; i < snap.length; i++) {
            if (snap[i] == 0) {
                defect = i;
            }

            if (snap[i] > 1) {
                repeat = i;
            }
        }

        return new int[]{repeat, defect};
    }


    /**
     * <p>题目：寻找重复数</p>
     * <ul>
     *     <li>来源：Leet Code </li>
     *     <li>题号：287</li>
     *     <li>难度: <span style="color:orange">中等</span></li>
     *     <li>题目：给定一个包含n + 1 个整数的数组nums ，其数字都在[1, n]范围内（包括 1 和 n），可知至少存在一个重复的整数。
     *              假设 nums 只有 一个重复的整数 ，返回这个重复的数 。
     *              你设计的解决方案必须 不修改 数组 nums 且只用常量级 O(1) 的额外空间。
     *     </li>
     *     <li>
     *         <p>提示：</p>
     *         <ul>
     * 	          <li>1 <= n <= 10<sup>5</sup></li>
     * 	          <li>nums.length == n + 1</li>
     * 	          <li>1 <= nums[i] <= n</li>
     * 	          <li>nums 中 只有一个整数 出现 两次或多次 ，其余整数均只出现 一次</li>
     *          </ul>
     *     </li>
     * </ul>
     *
     * @param nums 数组
     */
    public static int findDuplicate(int[] nums) {
        // 二分查找法
        // 思路：元素范围是[1, n], 因为数组中有一个重复的整数，所以，nums的长度应该是n+1，
        // 这样就可以用数组[1, n]来作为基本数组，使用二分查找法，找出其中重复出现的元素
        int length = nums.length;
        int l = 1, r = length - 1, ans = Integer.MIN_VALUE;

        while (l <= r) {
            int mid = (l + r) >> 1, count = 0;

            for (int i = 0; i < length; i++) {
                if (nums[i] <= mid) {
                    count++;
                }
            }

            if (count <= mid) {
                l = mid + 1;
            } else {
                r = mid - 1;
                ans = mid;
            }
        }
        return ans;

        // 快慢指针
        // int slow = nums[0], fast = nums[nums[0]];
        // 第一个循环：判断是否成环
        // while (slow != fast) {
        //     slow = nums[slow];
        //     fast = nums[nums[fast]];
        // }
        // fast = 0;
        // 第二个循环，查找环的入口，因为入口就是相同元素
        // while (slow != fast) {
        //     slow = nums[slow];
        //     fast = nums[fast];
        // }
        // return slow;
    }

    /**
     * <p>题目：找到所有数组中消失的数字</p>
     * <ul>
     *     <li>来源：Leet Code </li>
     *     <li>题号：448</li>
     *     <li>难度: <span style="color:green">简单</span></li>
     *     <li>题目：给你一个含 n 个整数的数组 nums ，其中 nums[i] 在区间 [1, n] 内。
     *     请你找出所有在 [1, n] 范围内但没有出现在 nums 中的数字，并以数组的形式返回结果。
     *     </li>
     *     <li>
     *         <p>提示：</p>
     *         <ul>
     * 	          <li>n = nums.length</li>
     * 	          <li>1 <= n <= 10<sup>5</sup></li>
     * 	          <li>1 <= nums[i] <= n</li>
     *          </ul>
     *     </li>
     * </ul>
     *
     * @param nums 数组
     */
    public static List<Integer> findDisappearedNumbers(int[] nums) {
        int[] snap = new int[nums.length + 1];

        for (int num : nums) {
            snap[num]++;
        }

        List<Integer> res = new ArrayList<>();
        for (int index = 1; index < snap.length; index++) {
            if(snap[index] != 0) {
                continue;
            }

            res.add(index);
        }

        return res;
    }
}
