package main.datastructures.linkedlist;

/**
 * 算法使用的数据结构
 *
 * @author jingzepei
 * @version 1.0
 * @since 2022/November/12 17:03
 */
public class LinkedListNode {
    int val;
    LinkedListNode next;
    LinkedListNode after;

    public LinkedListNode(int val) {
        this.val = val;
    }

    public LinkedListNode(int val, LinkedListNode next) {
        this.val = val;
        this.next = next;
    }

    @Override
    public String toString() {
        return "LinkedListNode{" + "val=" + val + ", next=" + next + ", after=" + after + '}';
    }
}
