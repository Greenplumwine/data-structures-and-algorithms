package main.datastructures.linkedlist;

import java.util.StringJoiner;

/**
 * 链表算法启动类
 *
 * @author jingzepei
 * @version 1.0
 * @since 2022/November/12 16:28
 */
public class Application {
    public static void main(String[] args) {
        // 链表相交
        // LinkedListNode common = new LinkedListNode(8);
        // common.next = new LinkedListNode(4);
        // common.next.next = new LinkedListNode(5);
        //
        // LinkedListNode list1 = new LinkedListNode(4);
        // list1.next = new LinkedListNode(1);
        // list1.next.next = common;
        //
        // LinkedListNode list2 = new LinkedListNode(5);
        // list2.next = new LinkedListNode(6);
        // list2.next.next = new LinkedListNode(1);
        // list2.next.next.next = common;
        //
        // System.out.println(getIntersectionNode(list1, list2).val);


        // 翻转链表
        // LinkedListNode list = new LinkedListNode(1);
        // list.next = new LinkedListNode(2);
        // list.next.next = new LinkedListNode(3);
        // list.next.next.next = new LinkedListNode(4);
        //
        //
        // System.out.println(reverseList(list));

        // 合并两个有序链表
        // LinkedListNode list1 = new LinkedListNode(1);
        // list1.next = new LinkedListNode(2);
        // list1.next.next = new LinkedListNode(4);
        //
        // LinkedListNode list2 = new LinkedListNode(1);
        // list2.next = new LinkedListNode(3);
        // list2.next.next = new LinkedListNode(4);
        //
        // System.out.println(mergeTwoLists(list1, list2));


        // 链表去重
        // LinkedListNode list = new LinkedListNode(1);
        // list.next = new LinkedListNode(1);
        // list.next.next = new LinkedListNode(2);
        // list.next.next.next = new LinkedListNode(2);
        // list.next.next.next.next = new LinkedListNode(3);
        //
        // System.out.println(deleteDuplicates(list));

        // 删除倒数第n个节点
        // LinkedListNode list = new LinkedListNode(1);
        // list.next = new LinkedListNode(2);
        // list.next.next = new LinkedListNode(3);
        // list.next.next.next = new LinkedListNode(4);
        // list.next.next.next.next = new LinkedListNode(5);
        //
        // System.out.println(removeNthFromEnd(list, 2));

        // 两两交换链表中的节点
        // LinkedListNode list = new LinkedListNode(1);
        // list.next = new LinkedListNode(2);
        // list.next.next = new LinkedListNode(3);
        // list.next.next.next = new LinkedListNode(4);
        // System.out.println(swapPairs(list));


        // 链表，两数相加
        LinkedListNode list1 = new LinkedListNode(7);
        list1.next = new LinkedListNode(2);
        list1.next.next = new LinkedListNode(4);
        list1.next.next.next = new LinkedListNode(3);

        LinkedListNode list2 = new LinkedListNode(5);
        list2.next = new LinkedListNode(6);
        list2.next.next = new LinkedListNode(4);

        LinkedListNode res = addTwoNumbers(list1, list2);

        StringJoiner joiner = new StringJoiner("->");

        while(res != null) {
            joiner.add(String.valueOf(res.val));
            res = res.next;
        }

        System.out.println(joiner);

        // 判断是否为回文链表
        // LinkedListNode list1 = new LinkedListNode(1);
        // list1.next = new LinkedListNode(0);
        // list1.next.next = new LinkedListNode(1);
        // System.out.println(isPalindrome(list1));

        // LinkedListNode list1 = new LinkedListNode(1);
        // list1.next = new LinkedListNode(2);
        // list1.next.next = new LinkedListNode(3);
        // list1.next.next.next = new LinkedListNode(4);
        // list1.next.next.next.next = new LinkedListNode(5);
        // System.out.println(oddEvenList(list1));
    }

    /**
     * <p>题目：相交链表</p>
     * <ul>
     *     <li>来源：Leet Code </li>
     *     <li>题号：160</li>
     *     <li>难度: <span style="color:green">简单</span></li>
     *     <li>
     *         题目：给你两个单链表的头节点 headA 和 headB ，请你找出并返回两个单链表相交的起始节点。如果两个链表不存在相交节点，返回 null 。
     *         题目数据 保证 整个链式结构中不存在环。
     *         注意，函数返回结果后，链表必须 保持其原始结构
     *     </li>
     *     <li>
     *         <p>提示：</p>
     *         <ul>
     *         	    <li>listA 中节点数目为m</li>
     *         	    <li>listB 中节点数目为n</li>
     *         	    <li>1 <= m, n <= 3 * 10<sup>4</sup></li>
     *         	    <li>1 <= Node.val <= 10<sup>5</sup></li>
     *         	    <li>0 <= skipA <= m</li>
     *         	    <li>0 <= skipB <= n</li>
     *         	    <li>如果listA 和listB 没有交点，intersectVal 为0</li>
     *         	    <li>如果listA 和listB 有交点，intersectVal == listA[skipA] == listB[skipB]</li>
     *         </ul>
     *     </li>
     * </ul>
     *
     * @param headA
     * @param headB
     */
    public static LinkedListNode getIntersectionNode(LinkedListNode headA, LinkedListNode headB) {
        LinkedListNode l1 = headA, l2 = headB;

        while (l1 != l2) {
            l1 = (l1 == null) ? headB : l1.next;
            l2 = (l2 == null) ? headA : l2.next;
        }

        return l1;
    }


    /**
     * <p>题目：反转链表</p>
     * <ul>
     *     <li>来源：Leet Code </li>
     *     <li>题号：206</li>
     *     <li>难度: <span style="color:green">简单</span></li>
     *     <li>
     *         题目：给你单链表的头节点 head ，请你反转链表，并返回反转后的链表。
     *     </li>
     *     <li>
     *         <p>提示：</p>
     *         <ul>
     * 	            <li>链表中节点的数目范围是[0, 5000]</li>
     * 	            <li>-5000 <= Node.val <= 5000</li>
     *         </ul>
     *     </li>
     * </ul>
     *
     * @param head
     */
    public static LinkedListNode reverseList(LinkedListNode head) {
        // 头插法实现
        // LinkedListNode newHead = new LinkedListNode(-1);
        //
        // while (head != null) {
        //     LinkedListNode next = head.next;
        //     head.next = newHead.next;
        //     newHead.next = head;
        //     head = next;
        // }
        // return newHead.next;


        // 递归实现
        if (head == null || head.next == null) {
            return head;
        }

        LinkedListNode next = head.next;
        LinkedListNode newHead = reverseList(next);

        next.next = head;
        head.next = null;

        return newHead;
    }

    /**
     * <p>题目：合并两个有序链表</p>
     * <ul>
     *     <li>来源：Leet Code </li>
     *     <li>题号：21</li>
     *     <li>难度: <span style="color:green">简单</span></li>
     *     <li>
     *         题目：将两个升序链表合并为一个新的 升序 链表并返回。新链表是通过拼接给定的两个链表的所有节点组成的。
     *     </li>
     *     <li>
     *         <p>提示：</p>
     *         <ul>
     * 	            <li>两个链表的节点数目范围是 [0, 50]</li>
     * 	            <li>-100 <= Node.val <= 100</li>
     * 	            <li>l1 和 l2 均按 <strong>非递减顺序</strong> 排列</li>
     *         </ul>
     *     </li>
     * </ul>
     *
     * @param list1
     * @param list2
     */
    public static LinkedListNode mergeTwoLists(LinkedListNode list1, LinkedListNode list2) {
        if (list1 == null) {
            return list2;
        }

        if (list2 == null) {
            return list1;
        }

        if (list1.val < list2.val) {
            list1.next = mergeTwoLists(list1.next, list2);
            return list1;
        } else {
            list2.next = mergeTwoLists(list1, list2.next);
            return list2;
        }
    }

    /**
     * <p>题目：删除排序链表中的重复元素</p>
     * <ul>
     *     <li>来源：Leet Code </li>
     *     <li>题号：83</li>
     *     <li>难度: <span style="color:green">简单</span></li>
     *     <li>
     *         题目：给定一个已排序的链表的头 head ， 删除所有重复的元素，使每个元素只出现一次 。返回 已排序的链表 。
     *     </li>
     *     <li>
     *         <p>提示：</p>
     *         <ul>
     * 	            <li>链表中节点数目在范围 [0, 300] 内</li>
     * 	            <li>-100 <= Node.val <= 100</li>
     * 	            <li>题目数据保证链表已经按升序 <strong>排列</strong></li>
     *          </ul>
     *     </li>
     * </ul>
     *
     * @param head 需要去重的链表
     */
    public static LinkedListNode deleteDuplicates(LinkedListNode head) {
        if (head == null || head.next == null) {
            return head;
        }

        head.next = deleteDuplicates(head.next);

        return (head.val == head.next.val) ? head.next : head;
    }


    /**
     * <p>题目：删除链表的倒数第 N 个结点</p>
     * <ul>
     *     <li>来源：Leet Code </li>
     *     <li>题号：19</li>
     *     <li>难度: <span style="color:green">简单</span></li>
     *     <li>
     *         题目：给你一个链表，删除链表的倒数第 n 个结点，并且返回链表的头结点。
     *     </li>
     *     <li>
     *         <p>提示：</p>
     *         <ul>
     * 	            <li>链表中结点的数目为 sz</li>
     * 	            <li>1 <= sz <= 30</li>
     * 	            <li>0 <= Node.val <= 100</li>
     * 	            <li>1 <= n <= sz</li>
     *         </ul>
     *     </li>
     * </ul>
     *
     * @param head 需要去重的链表
     */
    public static LinkedListNode removeNthFromEnd(LinkedListNode head, int n) {
        LinkedListNode fast = head;
        // 找出需要删除节点的前驱节点
        while (n-- > 0) {
            fast = fast.next;
        }

        if (fast == null) {
            return head.next;
        }

        LinkedListNode slow = head;

        while (fast.next != null) {
            fast = fast.next;
            slow = slow.next;
        }

        slow.next = slow.next.next;


        return head;
    }


    /**
     * <p>题目：两两交换链表中的节点</p>
     * <ul>
     *     <li>来源：Leet Code </li>
     *     <li>题号：24</li>
     *     <li>难度: <span style="color:orange">中等</span></li>
     *     <li>
     *         给你一个链表，两两交换其中相邻的节点，并返回交换后链表的头节点。你必须在不修改节点内部的值的情况下完成本题（即，只能进行节点交换）。
     *     </li>
     *     <li>
     *         <p>提示：</p>
     *         <ul>
     * 	            <li>链表中节点的数目在范围 [0, 100] 内</li>
     * 	            <li>0 <= Node.val <= 100</li>
     *         </ul>
     *     </li>
     * </ul>
     *
     * @param head 原数据
     */
    public static LinkedListNode swapPairs(LinkedListNode head) {
        // 非递归形式实现
        LinkedListNode pre = new LinkedListNode(-1);
        pre.next = head;
        LinkedListNode temp = pre;

        while (temp.next != null && temp.next.next != null) {
            LinkedListNode start = temp.next, end = temp.next.next;

            temp.next = end;
            start.next = end.next;
            end.next = start;
            temp = start;
        }
        return pre.next;

        // 递归形式实现
        // if (head == null || head.next == null) {
        //     return head;
        // }
        //
        // LinkedListNode next = head.next;
        //
        // head.next = swapPairs(next.next);
        // next.next = head;
        //
        // return next;
    }


    /**
     * <p>题目：两数相加 II</p>
     * <ul>
     *     <li>来源：Leet Code </li>
     *     <li>题号：445</li>
     *     <li>难度: <span style="color:orange">中等</span></li>
     *     <li>
     *         给你两个 非空 链表来代表两个非负整数。数字最高位位于链表开始位置。它们的每个节点只存储一位数字。将这两数相加会返回一个新的链表。
     *         你可以假设除了数字 0 之外，这两个数字都不会以零开头。
     *     </li>
     *     <li>
     *         <p>提示：</p>
     *         <ul>
     * 	            <li>链表的长度范围为 [1, 100]</li>
     * 	            <li>0 <= node.val <= 9</li>
     * 	            <li>输入数据保证链表代表的数字无前导 0</li>
     *         </ul>
     *     </li>
     *     <li>进阶：如果输入链表不能翻转该如何解决？</li>
     * </ul>
     *
     * @param l1
     * @param l2
     */
    static int cur = 0;
    public static LinkedListNode addTwoNumbers(LinkedListNode l1, LinkedListNode l2) {
        l1 = reverse(l1);
        l2 = reverse(l2);

        return add(l1, l2);
    }

    private static LinkedListNode reverse(LinkedListNode head) {
        if (head == null || head.next == null) {
            return head;
        }

        LinkedListNode next = head.next;
        LinkedListNode newHead = reverse(next);

        next.next = head;
        head.next = null;

        return newHead;
    }

    private static LinkedListNode add(LinkedListNode l1, LinkedListNode l2) {
        if (l1 == null) {
            return l2;
        }

        if (l2 == null) {
            return l1;
        }
        int sum = l1.val + l2.val + cur;
        cur = sum / 10;

        LinkedListNode head = new LinkedListNode(sum % 10);
        head.next = add(l1.next, l2.next);

        return head;
    }


    /**
     * <p>题目：回文链表</p>
     * <ul>
     *     <li>来源：Leet Code </li>
     *     <li>题号：234</li>
     *     <li>难度: <span style="color:green">简单</span></li>
     *     <li>
     *         给你一个单链表的头节点 head ，请你判断该链表是否为回文链表。如果是，返回 true ；否则，返回 false 。
     *     </li>
     *     <li>
     *         <p>提示：</p>
     *         <ul>
     *              <li>链表中节点数目在范围[1, 10<sup>5</sup>] 内</li>
     *              <li>0 <= Node.val <= 9</li>
     *         </ul>
     *     </li>
     *     <li>进阶：你能否用 O(n) 时间复杂度和 O(1) 空间复杂度解决此题？</li>
     * </ul>
     *
     * @param head
     */
    public static boolean isPalindrome(LinkedListNode head) {
        // 空和只有一个元素都属于回文
        if (head == null || head.next == null) {
            return true;
        }

        // 1. 找到后半部分的开头和结束
        LinkedListNode slow = head, fast = head.next;

        while (fast != null && fast.next != null) {
            slow = slow.next;
            fast = fast.next.next;
        }

        if (fast != null) {
            slow = slow.next;
        }

        // 2. 翻转后半部分的链表
        slow = reverseList(slow);

        // 3. 元素对比
        while (head != null && slow != null) {
            if (head.val != slow.val) {
                return false;
            }

            head = head.next;
            slow = slow.next;
        }

        return true;
    }

    /**
     * <p>题目：分隔链表</p>
     * <ul>
     *     <li>来源：Leet Code </li>
     *     <li>题号：725</li>
     *     <li>难度: <span style="color:orange">中等</span></li>
     *     <li>
     *         给你一个头结点为 head 的单链表和一个整数 k ，请你设计一个算法将链表分隔为 k 个连续的部分。
     *         每部分的长度应该尽可能的相等：任意两部分的长度差距不能超过 1 。这可能会导致有些部分为 null 。
     *         这 k 个部分应该按照在链表中出现的顺序排列，并且排在前面的部分的长度应该大于或等于排在后面的长度。
     *         返回一个由上述 k 部分组成的数组。
     *     </li>
     *     <li>
     *         <p>提示：</p>
     *         <ul>
     *              <li>链表中节点数目在范围[1, 1000] 内</li>
     *              <li>0 <= Node.val <= 1000</li>
     *              <li>0 <= k <= 50</li>
     *         </ul>
     *     </li>
     * </ul>
     *
     * @param head
     */
    public LinkedListNode[] splitListToParts(LinkedListNode head, int k) {
        int length = 0;

        LinkedListNode snap = head;
        // 计算链表的长度
        while (snap != null) {
            length++;
            snap = snap.next;
        }
        // 计算每个链表平均有多少个节点，还有多少没有处理
        int num = length / k, maxNum = length % k;

        LinkedListNode[] result = new LinkedListNode[k];

        snap = head;

        for (int i = 0; snap != null && i < k; i++) {
            result[i] = snap;
            // 计算每个链表应该包含的元素数量，因为可能存在余数，且每部分的长度相差不超过， 所以余数每次-1
            int curSize = num + ((maxNum-- > 0) ? 1 : 0);

            for (int j = 0; j < curSize - 1; j++) {
                snap = snap.next;
            }

            LinkedListNode next = snap.next;
            snap.next = null;
            snap = next;
        }

        return result;
    }



    /**
     * <p>题目：奇偶链表</p>
     * <ul>
     *     <li>来源：Leet Code </li>
     *     <li>题号：328</li>
     *     <li>难度: <span style="color:orange">中等</span></li>
     *     <li>
     *         给定单链表的头节点head，将所有<strong>索引</strong>为奇数的节点和<strong>索引</strong>为偶数的节点分别组合在一起，然后返回重新排序的列表。
     *         第一个节点的索引被认为是 奇数 ， 第二个节点的索引为偶数，以此类推。
     *         请注意，偶数组和奇数组内部的相对顺序应该与输入时保持一致。
     *         你必须在O(1)的额外空间复杂度和O(n)的时间复杂度下解决这个问题。
     *     </li>
     *     <li>
     *         <p>提示：</p>
     *         <ul>
     * 	        <li>n == 链表中的节点数</li>
     * 	        <li>0 <= n <= 10<sup>4</sup></li>
     * 	        <li>-10<sup>6</sup> <= Node.val <= 10<sup>6</sup></li>
     *         </ul>
     *     </li>
     * </ul>
     *
     * @param head
     */
    public static LinkedListNode oddEvenList(LinkedListNode head) {
        if (head == null || head.next == null) {
            return head;
        }

        LinkedListNode odd = head, even = head.next, evenHead = even;

        while (even != null && even.next != null) {
            odd.next = odd.next.next;
            even.next = even.next.next;
            odd = odd.next;
            even = even.next;
        }

        odd.next = evenHead;

        return head;
    }
}
