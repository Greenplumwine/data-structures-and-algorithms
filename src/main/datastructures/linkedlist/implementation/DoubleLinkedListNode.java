package main.datastructures.linkedlist.implementation;

import java.util.Objects;

/**
 * 双向链表节点
 *
 * @author jingzepei
 * @version 1.0
 * @since 2022/November/12 16:38
 */
public class DoubleLinkedListNode<E> {
    /**
     * 链表元素
     */
    private E value;

    /**
     * 双向链表的前驱节点
     */
    private DoubleLinkedListNode<E> after;

    /**
     * 双向链表的后驱节点
     */
    private DoubleLinkedListNode<E> next;

    public DoubleLinkedListNode(E value) {
        this.value = value;
    }

    public DoubleLinkedListNode(E value, DoubleLinkedListNode<E> after, DoubleLinkedListNode<E> next) {
        this.value = value;
        this.after = after;
        this.next = next;
    }

    public E getValue() {
        return value;
    }

    public void setValue(E value) {
        this.value = value;
    }

    public DoubleLinkedListNode<E> getAfter() {
        return after;
    }

    public void setAfter(DoubleLinkedListNode<E> after) {
        this.after = after;
    }

    public DoubleLinkedListNode<E> getNext() {
        return next;
    }

    public void setNext(DoubleLinkedListNode<E> next) {
        this.next = next;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        DoubleLinkedListNode<?> that = (DoubleLinkedListNode<?>) o;

        if (!Objects.equals(value, that.value)) {
            return false;
        }
        if (!Objects.equals(after, that.after)) {
            return false;
        }
        return Objects.equals(next, that.next);
    }

    @Override
    public int hashCode() {
        int result = value != null ? value.hashCode() : 0;
        result = 31 * result + (after != null ? after.hashCode() : 0);
        result = 31 * result + (next != null ? next.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "DoubleLinkedListNode{" + "value=" + value + ", after=" + after + ", next=" + next + '}';
    }
}
