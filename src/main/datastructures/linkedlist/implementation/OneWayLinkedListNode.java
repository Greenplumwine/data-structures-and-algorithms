package main.datastructures.linkedlist.implementation;

import java.util.Objects;

/**
 * 单向链表节点
 *
 * @author jingzepei
 * @version 1.0
 * @since 2022/November/12 16:38
 */
public class OneWayLinkedListNode<E> {
    /**
     * 链表元素
     */
    private E value;

    /**
     * 单向链表的后驱节点
     */
    private OneWayLinkedListNode<E> next;

    public OneWayLinkedListNode(E value) {
        this.value = value;
    }

    public OneWayLinkedListNode(E value, OneWayLinkedListNode<E> next) {
        this.value = value;
        this.next = next;
    }

    public E getValue() {
        return value;
    }

    public void setValue(E value) {
        this.value = value;
    }

    public OneWayLinkedListNode<E> getNext() {
        return next;
    }

    public void setNext(OneWayLinkedListNode<E> next) {
        this.next = next;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        OneWayLinkedListNode<?> that = (OneWayLinkedListNode<?>) o;

        if (!Objects.equals(value, that.value)) {
            return false;
        }
        return Objects.equals(next, that.next);
    }

    @Override
    public int hashCode() {
        int result = value != null ? value.hashCode() : 0;
        result = 31 * result + (next != null ? next.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "OneWayLinkedListNode{" + "value=" + value + ", next=" + next + '}';
    }
}
