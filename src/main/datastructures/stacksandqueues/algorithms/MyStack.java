package main.datastructures.stacksandqueues.algorithms;

import java.util.LinkedList;
import java.util.Queue;

/**
 * <p>题目：用队列实现栈</p>
 * <ul>
 *     <li>来源：Leet Code </li>
 *     <li>题号：225</li>
 *     <li>难度: <span style="color:green">简单</span></li>
 *     <li>
 *         <p>请你仅使用两个队列实现一个后入先出（LIFO）的栈，并支持普通栈的全部四种操作（push、top、pop 和 empty）。</p>
 *         <p>实现 MyStack 类：</p>
 *         <ul>
 * 	           <li>void push(int x) 将元素 x 压入栈顶。</li>
 * 	           <li>int pop() 移除并返回栈顶元素。</li>
 * 	           <li>int peek() 返回栈顶元素。</li>
 * 	           <li>boolean empty() 如果栈是空的，返回 true ；否则，返回 false</li>
 *         </ul>
 *         <p><strong>说明：</strong></p>
 *         <ul>
 * 	            <li>你只能使用队列的基本操作 —— 也就是 push to back、peek/pop from front、size 和 is empty 这些操作。</li>
 * 	            <li>你所使用的语言也许不支持队列。 你可以使用 list （列表）或者 deque（双端队列）来模拟一个队列 , 只要是标准的队列操作即可。</li>
 *         </ul>
 *     </li>
 *     <li>
 *         <p>提示：</p>
 *         <ul>
 * 	            <li>1 <= x <= 9</li>
 * 	            <li>最多调用 100 次 push、pop、peek 和 empty</li>
 * 	            <li>每次调用 pop 和 top 都保证栈不为空</li>
 *         </ul>
 *     </li>
 *     <li>
 *         <p>进阶：你能否仅用一个队列来实现栈。</p>
 *     </li>
 * </ul>
 *
 * @author jingzepei
 * @version 1.0
 * @since 2022-11-14
 */
public class MyStack {
    private Queue<Integer> values;

    public MyStack() {
        this.values = new LinkedList<>();
    }

    public void push(int x) {
        values.add(x);

        int size = this.values.size();

        while (size-- > 1) {
            values.add(values.poll());
        }
    }

    public int pop() {
        return this.values.remove();
    }

    public int top() {
        return this.values.peek();
    }

    public boolean empty() {
        return this.values.isEmpty();
    }

    public static void main(String[] args) {
        MyStack obj = new MyStack();
        obj.push(1);
        int param_2 = obj.pop();
        int param_3 = obj.top();
        boolean param_4 = obj.empty();
    }
}
