package main.datastructures.stacksandqueues.algorithms;

import java.util.Arrays;
import java.util.Stack;

/**
 * 其他算法
 *
 * @author jingzepei
 * @version 1.0
 * @since 2022-11-14 17:32
 */
public class Application {
    public static void main(String[] args) {
        // String s = "(]";
        // System.out.println(isValid(s));

        int[] aa = new int[] {1, 2, 1};

        int[] res = nextGreaterElements(aa);

        for (int i = 0; i < res.length; i++) {
            System.out.println(res[i] + "\t");
        }
    }

    /**
     * <p>题目：有效的括号</p>
     * <ul>
     *     <li>来源：Leet Code </li>
     *     <li>题号：20</li>
     *     <li>难度: <span style="color:green">简单</span></li>
     *     <li>
     *         <p>给定一个只包括 '('，')'，'{'，'}'，'['，']' 的字符串 s ，判断字符串是否有效。</p>
     *         <p>有效字符串需满足：</p>
     *         <ol>
     * 	            <li>左括号必须用相同类型的右括号闭合。</li>
     * 	            <li>左括号必须以正确的顺序闭合。</li>
     * 	            <li>每个右括号都有一个对应的相同类型的左括号。</li>
     *         </ol>
     *     </li>
     *     <li>
     *         <p>提示：</p>
     *         <ul>
     *              <li>1 <= s.length <= 10<sup>4</sup></li>
     *              <li>s 仅由括号 '()[]{}' 组成</li>
     *         </ul>
     *     </li>
     * </ul>
     *
     * @param s
     */
    public static boolean isValid(String s) {
        Stack<Character> snap = new Stack<>();

        for (char c : s.toCharArray()) {
            if (c == '(' || c == '{' || c == '[') {
                snap.push(c);
                continue;
            }

            if (snap.isEmpty()) {
                return false;
            }

            int nextAscii = snap.pop() + 1;
            boolean close = (nextAscii == c || (nextAscii + 1) == c);

            if (!close) {
                return false;
            }
        }

        return snap.isEmpty();
    }


    /**
     * <p>题目：每日温度</p>
     * <ul>
     *     <li>来源：Leet Code </li>
     *     <li>题号：739</li>
     *     <li>难度: <span style="color:orange">中等</span></li>
     *     <li>
     *         给定一个整数数组temperatures，表示每天的温度，返回一个数组answer，其中answer[i]是指对于第 i 天，下一个更高温度出现在几天后。如果气温在这之后都不会升高，请在该位置用0 来代替。
     *     </li>
     *     <li>
     *         <p>提示：</p>
     *         <ul>
     *              <li>1 <= temperatures.length <= 10<sup>5</sup></li>
     *              <li>30 <= temperatures[i] <= 100</li>
     *         </ul>
     *     </li>
     * </ul>
     *
     * @param temperatures 每天的温度
     */
    public int[] dailyTemperatures(int[] temperatures) {

        // todo 速度快，执行返回的时间为8ms
        // 动态规划逻辑
        int[] res = new int[temperatures.length];
        // 因为最后一个元素后面没有任何元素，所以无法找到比他大的下一个元素，因此，直接为0即可
        res[temperatures.length - 1] = 0;

        for (int i = temperatures.length - 2; i >= 0; i--) {
            // 只要这个元素后面的数据大于当前元素，直接返回1即可
            if (temperatures[i] < temperatures[i + 1]) {
                res[i] = 1;
            } else if (temperatures[i] >= temperatures[i + 1]) {
                // 如果是当前元素小于或等于后一个的元素，直接根据后一个元素的数量去比较比后一个元素大的元素
                // 原因 B < C 且 A > B，就可以直接跳过中间所有元素，去找比他大的元素进行对比
                int j = i + 1;
                while (res[j] != 0) {
                    j += res[j];
                    if (temperatures[i] < temperatures[j]) {
                        res[i] = j - i;
                        break;
                    }
                }
            }
        }

        return res;

        // 单调栈实现
        // todo 速度太慢，执行返回的时间为161ms
        // int[] res = new int[temperatures.length];
        //
        // Stack<Integer> snap = new Stack<>();
        // for (int i = 0; i < temperatures.length; i++) {
        //    while (!snap.isEmpty() && temperatures[i] > temperatures[snap.peek()]) {
        //        int preIndex = snap.pop();
        //        res[preIndex] = i - preIndex;
        //    }
        //
        //    snap.add(i);
        // }
        //
        // return res;
    }


    /**
     * <p>题目：下一个更大元素 II</p>
     * <ul>
     *     <li>来源：Leet Code </li>
     *     <li>题号：503</li>
     *     <li>难度: <span style="color:orange">中等</span></li>
     *     <li>
     *         给定一个循环数组nums（nums[nums.length - 1]的下一个元素是nums[0]），返回nums中每个元素的 下一个更大元素 。
     *          数字 x的 下一个更大的元素 是按数组遍历顺序，这个数字之后的第一个比它更大的数，这意味着你应该循环地搜索它的下一个更大的数。如果不存在，则输出 -1。
     *     </li>
     *     <li>
     *         <p>提示：</p>
     *         <ul>
     *              <li>1 <= temperatures.length <= 10<sup>4</sup></li>
     *              <li>-10<sup>9</sup> <= temperatures[i] <= 10<sup>9</sup></li>
     *         </ul>
     *     </li>
     * </ul>
     *
     * @param nums 循环数组 
     */
    public static int[] nextGreaterElements(int[] nums) {

        // 单调栈
        // todo 速度较快, 26ms
        int[] res = new int[nums.length];
        Arrays.fill(res, -1);

        Stack<Integer> snap = new Stack<>();

        for (int i = 0; i < nums.length * 2; i++) {
            int num = nums[i % nums.length];

            while (!snap.isEmpty() && nums[snap.peek()] < num) {
                res[snap.pop()] = num;
            }

            if (i < nums.length) {
                snap.push(i);
            }
        }

        return res;


        // 暴力模式
        // todo 速度太慢, 71ms
        // for (int i = 0; i < nums.length; i++) {
        //     int num = nums[i];
        //     for (int j = (i + 1 < nums.length ? i + 1 : 0); j < nums.length; j++) {
        //         if (j == i) {
        //             res[i] = -1;
        //             break;
        //         }
        //         int nextNum = nums[j];
        //         if (num < nextNum) {
        //             res[i] = nextNum;
        //             break;
        //         } else if (j >= nums.length - 1){
        //             j = -1;
        //         }
        //     }
        // }
        //
        // return res;
    }
}
