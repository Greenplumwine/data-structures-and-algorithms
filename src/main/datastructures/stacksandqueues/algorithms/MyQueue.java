package main.datastructures.stacksandqueues.algorithms;

import java.util.Stack;


/**
 * <p>题目：用栈实现队列</p>
 * <ul>
 *     <li>来源：Leet Code </li>
 *     <li>题号：232</li>
 *     <li>难度: <span style="color:green">简单</span></li>
 *     <li>
 *         <p>请你仅使用两个栈实现先入先出队列。队列应当支持一般队列支持的所有操作（push、pop、peek、empty）：</p>
 *         <p>实现 MyQueue 类：</p>
 *         <ul>
 * 	           <li>void push(int x) 将元素 x 推到队列的末尾</li>
 * 	           <li>int pop() 从队列的开头移除并返回元素</li>
 * 	           <li>int peek() 返回队列开头的元素</li>
 * 	           <li>boolean empty() 如果队列为空，返回 true ；否则，返回 false</li>
 *         </ul>
 *         <p><strong>说明：</strong></p>
 *         <ul>
 * 	            <li>你 <strong>只能</strong> 使用标准的栈操作 —— 也就是只有push to top,peek/pop from top,size, 和is empty操作是合法的。</li>
 * 	            <li>你所使用的语言也许不支持栈。你可以使用 list 或者 deque（双端队列）来模拟一个栈，只要是标准的栈操作即可。</li>
 *         </ul>
 *     </li>
 *     <li>
 *         <p>提示：</p>
 *         <ul>
 * 	            <li>1 <= x <= 9</li>
 * 	            <li>最多调用 100 次 push、pop、peek 和 empty</li>
 * 	            <li>假设所有操作都是有效的 （例如，一个空的队列不会调用 pop 或者 peek 操作）</li>
 *         </ul>
 *     </li>
 *     <li>
 *         <p>进阶：你能否实现每个操作均摊时间复杂度为 O(1) 的队列？换句话说，执行 n 个操作的总时间复杂度为 O(n) ，即使其中一个操作可能花费较长时间。</p>
 *     </li>
 * </ul>
 *
 * @author jingzepei
 * @version 1.0
 * @since 2022-11-14
 */
public class MyQueue {
    /**
     * 用于输出结果的栈
     */
    private Stack<Integer> out;

    /**
     * 用于添加数据的栈
     */
    private Stack<Integer> in;


    public MyQueue() {
        this.out = new Stack<>();
        this.in = new Stack<>();
    }

    public void push(int x) {
        this.in.push(x);
    }

    public int pop() {
        inToOut();
        return out.pop();
    }

    public int peek() {
        inToOut();
        return out.peek();
    }

    private void inToOut() {
        if (out.isEmpty()) {
            while (!in.isEmpty()) {
                out.push(in.pop());
            }
        }
    }

    public boolean empty() {
        return in.empty() && out.empty();
    }


    public static void main(String[] args) {
          MyQueue obj = new MyQueue();
          obj.push(1);
          int param_2 = obj.pop();
          int param_3 = obj.peek();
          boolean param_4 = obj.empty();
    }
}
