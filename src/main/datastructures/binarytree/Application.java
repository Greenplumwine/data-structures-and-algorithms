package main.datastructures.binarytree;

import java.util.ArrayList;
import java.util.List;

/**
 * @author jingzepei
 * @version 1.0
 * @since 2022-11-27 19:40
 */
public class Application {
    /**
     * leetcode 树的示例
     */
    public static class TreeNode {
        int val;
        TreeNode left;
        TreeNode right;

        TreeNode() {
        }

        TreeNode(int val) {
            this.val = val;
        }

        TreeNode(int val, TreeNode left, TreeNode right) {
            this.val = val;
            this.left = left;
            this.right = right;
        }
    }

    public class ListNode {
        int val;
        ListNode next;

        ListNode() {
        }

        ListNode(int val) {
            this.val = val;
        }

        ListNode(int val, ListNode next) {
            this.val = val;
            this.next = next;
        }
    }

    public static void main(String[] args) {
        Application application = new Application();
        TreeNode root = new TreeNode(1);
        TreeNode right = new TreeNode(2);
        TreeNode right1 = new TreeNode(2);
        root.right = right;
        root.right.right = right1;


        int[] ints = application.findMode(root);

        for (int anInt : ints) {
            System.out.println(anInt);
        }
    }

    /**
     * <p>题目：修剪二叉搜索树</p>
     * <ul>
     *     <li>来源：leet code </li>
     *     <li>题号：669</li>
     *     <li>难度: <span style="color:orange">中等</span></li>
     *     <li>
     *         给你二叉搜索树的根节点 root ，同时给定最小边界low 和最大边界 high。通过修剪二叉搜索树，
     *         使得所有节点的值在[low, high]中。修剪树 不应该改变保留在树中的元素的相对结构
     *         (即，如果没有被移除，原有的父代子代关系都应当保留)。 可以证明，存在唯一的答案。
     * <p>
     *         所以结果应当返回修剪好的二叉搜索树的新的根节点。注意，根节点可能会根据给定的边界发生改变。
     *     </li>
     *     <li>
     *         <p>提示：</p>
     *         <ul>
     * 	            <li>树中节点数在范围 [1, 10<sup>4</sup>] 内</li>
     * 	            <li>0 <= Node.val <= 10<sup>4</sup></li>
     * 	            <li>树中每个节点的值都是 <strong>唯一</strong> 的</li>
     * 	            <li>题目数据保证输入是一棵有效的二叉搜索树</li>
     * 	            <li>0 <= low <= high <= 10<sup>4</sup></li>
     *         </ul>
     *     </li>
     * </ul>
     *
     * @param root 一棵有效的二叉搜索树
     * @param low  最小边界
     * @param high 最大边界
     */
    public static TreeNode trimBST(TreeNode root, int low, int high) {
        if (root == null) {
            return null;
        }

        if (root.val > high) {
            return trimBST(root.left, low, high);
        }

        if (root.val < low) {
            return trimBST(root.right, low, high);
        }

        root.left = trimBST(root.left, low, high);
        root.right = trimBST(root.right, low, high);

        return root;
    }

    /**
     * <p>题目：二叉搜索树中第K小的元素</p>
     * <ul>
     *     <li>来源：leet code </li>
     *     <li>题号：230</li>
     *     <li>难度: <span style="color:orange">中等</span></li>
     *     <li>
     *         给定一个二叉搜索树的根节点 root ，和一个整数 k ，请你设计一个算法查找其中第 k 个最小元素（从 1 开始计数）。
     *     </li>
     *     <li>
     *         <p>提示：</p>
     *         <ul>
     * 	            <li>树中节点数为n</li>
     * 	            <li>0 <= Node.val <= 10<sup>4</sup></li>
     * 	            <li>0 <= k <= n <= 10<sup>4</sup></li>
     *         </ul>
     *     </li>
     * </ul>
     *
     * @param root 一棵有效的二叉搜索树
     * @param k  最小边界
     */
    static int count = 0;

    public static int kthSmallest(TreeNode root, int k) {
        // 深度优先遍历
        if (root == null) {
            return Integer.MIN_VALUE;
        }

        int res = kthSmallest(root.left, k);
        count++;
        if (count == k) {
            return root.val;
        }

        if (res == Integer.MIN_VALUE) {
            res = kthSmallest(root.right, k);
        }

        return res;
    }

    /**
     * <p>题目： 把二叉搜索树转换为累加树</p>
     * <ul>
     *     <li>来源：leet code </li>
     *     <li>题号：538</li>
     *     <li>难度: <span style="color:orange">中等</span></li>
     *     <li>
     *        给出二叉 搜索 树的根节点，该树的节点值各不相同，请你将其转换为累加树（Greater Sum Tree），
     *        使每个节点 node的新值等于原树中大于或等于node.val的值之和。
     *        提醒一下，二叉搜索树满足下列约束条件：
     *        <ul>
     *            <li>节点的左子树仅包含键 小于 节点键的节点。</li>
     *            <li>节点的右子树仅包含键 大于 节点键的节点。</li>
     *            <li>左右子树也必须是二叉搜索树。</li>
     *        </ul>
     *     </li>
     *     <li>
     *         <p>提示：</p>
     *         <ul>
     * 	           <li>树中的节点数介于 0和 10<sup>4</sup><sup></sup>之间。</li>
     * 	           <li>每个节点的值介于 -10<sup>4</sup>和10<sup>4</sup>之间。</li>
     * 	           <li>树中的所有值 <strong>互不相同</strong> 。</li>
     * 	           <li>给定的树为二叉搜索树。</li>
     *         </ul>
     *     </li>
     * </ul>
     *
     * @param root 一棵有效的二叉搜索树
     */
    static int sum = 0;

    public static TreeNode convertBST(TreeNode root) {
        addAll(root);
        return root;
    }

    public static void addAll(TreeNode node) {
        // 最开始没看懂"每个节点 node 的新值等于原树中大于或等于 node.val 的值之和。"是什么意思
        // 2022-11-28 想明白了,新的tree应该是左子树大于当前节点，右子树小于当前节点，相当于翻转了一下
        // 因为中序遍历是单调递增的，为了能够使其顺序翻转，所以我们需要进行单调递减，因此用反向的中序遍历就可以做到
        // 深度优先遍历
        if (node == null) {
            return;
        }

        addAll(node.right);
        sum += node.val;
        node.val = sum;
        addAll(node.left);
    }


    /**
     * <p>题目： 二叉搜索树的最近公共祖先</p>
     * <ul>
     *     <li>来源：leet code </li>
     *     <li>题号：235</li>
     *     <li>难度: <span style="color:orange">中等</span></li>
     *     <li>
     *        给定一个二叉搜索树, 找到该树中两个指定节点的最近公共祖先。
     *        百度百科中最近公共祖先的定义为：“对于有根树 T 的两个结点 p、q，最近公共祖先表示为一个结点 x，
     *        满足 x 是 p、q 的祖先且 x 的深度尽可能大（一个节点也可以是它自己的祖先）。”
     * <p>
     *        例如，给定如下二叉搜索树:root =[6,2,8,0,4,7,9,null,null,3,5]
     *     </li>
     *     <li>
     *         <p>提示：</p>
     *         <ul>
     * 	           <li>所有节点的值都是唯一的。</li>
     * 	           <li>p、q 为不同节点且均存在于给定的二叉搜索树中。</li>
     *         </ul>
     *     </li>
     * </ul>
     *
     * @param root 一棵有效的二叉搜索树
     */
    public TreeNode lowestCommonAncestor(TreeNode root, TreeNode p, TreeNode q) {
        // 深度优先遍历
        if (root.val > p.val && root.val > q.val) {
            return lowestCommonAncestor(root.left, p, q);
        }

        if (root.val < p.val && root.val < q.val) {
            return lowestCommonAncestor(root.right, p, q);
        }

        return root;
    }

    /**
     * <p>题目： 二叉树的最近公共祖先</p>
     * <ul>
     *     <li>来源：leet code </li>
     *     <li>题号：236</li>
     *     <li>难度: <span style="color:orange">中等</span></li>
     *     <li>
     *        给定一个二叉搜索树, 找到该树中两个指定节点的最近公共祖先。
     *        百度百科中最近公共祖先的定义为：“对于有根树 T 的两个结点 p、q，最近公共祖先表示为一个结点 x，
     *        满足 x 是 p、q 的祖先且 x 的深度尽可能大（一个节点也可以是它自己的祖先）。”
     *     </li>
     *     <li>
     *         <p>提示：</p>
     *         <ul>
     * 	            <li>树中节点数目在范围 [2, 10<sup>5</sup>] 内。</li>
     * 	            <li>-10<sup>9</sup> <= Node.val <= 10<sup>9</sup></li>
     * 	            <li>所有 Node.val 互不相同 。</li>
     * 	            <li>p != q</li>
     * 	            <li>p 和 q 均存在于给定的二叉树中。</li>
     *         </ul>
     *     </li>
     * </ul>
     *
     * @param root
     */
    public TreeNode lowestCommonAncestorByBinaryTree(TreeNode root, TreeNode p, TreeNode q) {
        // 深度优先遍历
        if (root == null || root == p || root == q) {
            return root;
        }

        TreeNode left = lowestCommonAncestorByBinaryTree(root.left, p, q);
        TreeNode right = lowestCommonAncestorByBinaryTree(root.right, p, q);

        return left == null ? right : right == null ? left : root;
    }


    /**
     * <p>题目： 将有序数组转换为二叉搜索树</p>
     * <ul>
     *     <li>来源：leet code </li>
     *     <li>题号：108</li>
     *     <li>难度: <span style="color:green">简单</span></li>
     *     <li>
     *         给你一个整数数组 nums ，其中元素已经按 升序 排列，请你将其转换为一棵 高度平衡 二叉搜索树。
     *         高度平衡 二叉树是一棵满足「每个节点的左右两个子树的高度差的绝对值不超过 1 」的二叉树。
     *     </li>
     *     <li>
     *         <p>提示：</p>
     *         <ul>
     * 	            <li>1 <= nums.length <= 10<sup>4</sup></li>
     * 	            <li>-10<sup>4</sup> <= nums[i] <= 10<sup>4</sup></li>
     * 	            <li>nums 按 <strong>严格递增</strong> 顺序排列</li>
     *         </ul>
     *     </li>
     * </ul>
     *
     * @param nums 有序数组
     */
    public TreeNode sortedArrayToBST(int[] nums) {
        return conversion(nums, 0, nums.length - 1);
    }

    public TreeNode conversion(int[] nums, int start, int end) {
        // 中序遍历，二分查找法找值
        if (start > end) {
            return null;
        }

        int middle = (start + end) / 2;
        TreeNode root = new TreeNode(nums[middle]);
        root.left = conversion(nums, start, middle - 1);
        root.right = conversion(nums, middle + 1, end);
        return root;
    }

    /**
     * <p>题目：有序链表转换二叉搜索树</p>
     * <ul>
     *     <li>来源：leet code </li>
     *     <li>题号：109</li>
     *     <li>难度: <span style="color:orange">中等</span></li>
     *     <li>
     *         给定一个单链表的头节点head，其中的元素 按升序排序 ，将其转换为高度平衡的二叉搜索树。
     *         本题中，一个高度平衡二叉树是指一个二叉树每个节点的左右两个子树的高度差不超过 1。
     *     </li>
     *     <li>
     *         <p>提示：</p>
     *         <ul>
     * 	          <li>head 中的节点数在[0, 2 * 10<sup>4</sup>]范围内</li>
     * 	          <li>-10<sup>5</sup> <= Node.val <= 10<sup>5</sup></li>
     *         </ul>
     *     </li>
     * </ul>
     *
     * @param head 链表
     */
    public TreeNode sortedListToBST(ListNode head) {
        // 中序遍历，二分查找法
        if (head == null) {
            return null;
        }

        if (head.next == null ) {
            return new TreeNode(head.val);
        }

        // 查找链表中间值
        ListNode middle = this.findMiddleListNode(head);
        ListNode next = middle.next;

        middle.next = null;

        TreeNode root = new TreeNode(next.val);
        root.left = this.sortedListToBST(head);
        root.right = this.sortedListToBST(next.next);

        return root;
    }

    /**
     * 查找链表中间节点
     * @param node
     * @return
     */
    public ListNode findMiddleListNode(ListNode node) {
        ListNode slow = node, fast = node.next, middle = slow;

        while (fast != null && fast.next != null) {
            middle = slow;
            slow = slow.next;
            fast = fast.next.next;
        }
        return middle;
    }


    /**
     * <p>题目：两数之和 IV - 输入二叉搜索树</p>
     * <ul>
     *     <li>来源：leet code </li>
     *     <li>题号：653</li>
     *     <li>难度: <span style="color:green">简单</span></li>
     *     <li>
     *         给定一个二叉搜索树 root 和一个目标结果 k，如果二叉搜索树中存在两个元素且它们的和等于给定的目标结果，则返回 true。
     *     </li>
     *     <li>
     *         <p>提示：</p>
     *         <ul>
     * 	            <li>二叉树的节点个数的范围是  [1, 10<sup>4</sup>].</li>
     * 	            <li>-10<sup>4</sup> <= Node.val <= 10<sup>4</sup></li>
     * 	            <li>题目数据保证，输入的 root 是一棵 <strong>有效</strong> 的二叉搜索树</li>
     * 	            <li>-10<sup>5</sup> <= k <= 10<sup>5</sup></li>
     *         </ul>
     *     </li>
     * </ul>
     *
     * @param root 二叉搜索树
     * @param k 两数之和的目标值
     */
    public boolean findTarget(TreeNode root, int k) {
        // 中序遍历，双指针
        List<Integer> snap = new ArrayList<>();

        middleOrderTraversal(root, snap);

        int start = 0, end = snap.size() - 1;

        while (start < end) {
            int sum = snap.get(start) + snap.get(end);
            if (sum == k) {
                return true;
            }

            if (sum < k) {
                start++;
            } else {
                end--;
            }
        }
        return false;
        // 层次遍历，速度:执行用时：17 ms, 在所有 Java 提交中击败了5.01%
        // if (root == null || (root.left == null && root.right == null)) {
        //     return false;
        // }
        //
        // List<Integer> array = new ArrayList<>();
        //
        // Queue<TreeNode> queue = new LinkedList<>();
        // queue.add(root);
        //
        // while (!queue.isEmpty()) {
        //     TreeNode node = queue.poll();
        //
        //     int poor = k - node.val;
        //
        //     if (array.contains(poor)) {
        //         return true;
        //     }
        //
        //     array.add(node.val);
        //
        //     if (node.left != null) {
        //         queue.add(node.left);
        //     }
        //
        //     if (node.right != null) {
        //         queue.add(node.right);
        //     }
        // }
        //
        // return false;
    }


    /**
     * 中序遍历
     * @param root
     * @param nums
     */
    public void middleOrderTraversal(TreeNode root, List<Integer> nums) {
        if (root == null) {
            return;
        }
        middleOrderTraversal(root.left, nums);
        nums.add(root.val);
        middleOrderTraversal(root.right, nums);
    }

    /**
     * <p>题目：二叉搜索树的最小绝对差</p>
     * <ul>
     *     <li>来源：leet code </li>
     *     <li>题号：530</li>
     *     <li>难度: <span style="color:green">简单</span></li>
     *     <li>
     *         给你一个二叉搜索树的根节点 root ，返回 树中任意两不同节点值之间的最小差值 。差值是一个正数，其数值等于两值之差的绝对值。
     *     </li>
     *     <li>
     *         <p>提示：</p>
     *         <ul>
     * 	            <li>二叉树的节点个数的范围是  [2, 10<sup>4</sup>].</li>
     * 	            <li>0 <= Node.val <= 10<sup>5</sup></li>
     *         </ul>
     *     </li>
     * </ul>
     *
     * @param root 二叉搜索树
     * @param k 两数之和的目标值
     */
    private int min = Integer.MAX_VALUE;
    private TreeNode after = null;
    public int getMinimumDifference(TreeNode root) {
        // 中序遍历
        inOrder(root);
        return min;
    }

    public void inOrder(TreeNode node) {
        if (node == null) {
            return;
        }

        inOrder(node.left);
        if (after != null) {
            min = Math.min(min, node.val - after.val);
        }

        after = node;

        inOrder(node.right);
    }


    /**
     * <p>题目：二叉搜索树中的众数</p>
     * <ul>
     *     <li>来源：leet code </li>
     *     <li>题号：501</li>
     *     <li>难度: <span style="color:green">简单</span></li>
     *     <li>
     *         给你一个含重复值的二叉搜索树（BST）的根节点 root ，找出并返回 BST 中的所有 众数（即，出现频率最高的元素）。
     *         如果树中有不止一个众数，可以按 任意顺序 返回。
     *         假定 BST 满足如下定义：
     *           结点左子树中所含节点的值 小于等于 当前节点的值
     *           结点右子树中所含节点的值 大于等于 当前节点的值
     *           左子树和右子树都是二叉搜索树
     *     </li>
     *     <li>
     *         <p>提示：</p>
     *         <ul>
     * 	            <li>二叉树的节点个数的范围是  [1, 10<sup>4</sup>].</li>
     * 	            <li>-10<sup>5</sup> <= Node.val <= 10<sup>5</sup></li>
     *         </ul>
     *     </li>
     *     <li>
     *         进阶：你可以不使用额外的空间吗？（假设由递归产生的隐式调用栈的开销不被计算在内）
     *     </li>
     * </ul>
     *
     * @param root 二叉搜索树
     * @param k 两数之和的目标值
     */
    private int maxCount = 1, nowCount = 1;
    private TreeNode preNode = null;
    public int[] findMode(TreeNode root) {
        // 中序遍历，在遍历的时候记录出现次数最大的数
        List<Integer> snap = new ArrayList<>();
        this.ss(root, snap);
        int[] res = new int[snap.size()];

        for (int i = 0; i < res.length; i++) {
            res[i] = snap.get(i);
        }

        return res;
    }

    public void ss(TreeNode root, List<Integer> snap) {
        if (root == null) {
            return;
        }

        ss(root.left, snap);

        if (preNode != null) {
            if (preNode.val == root.val) {
                nowCount++;
            } else {
                nowCount = 1;
            }
        }
        
        if (nowCount > maxCount || nowCount == maxCount) {
            if(nowCount > maxCount) {
                maxCount = nowCount;
                snap.clear();
            }

            snap.add(root.val);
        }
        preNode = root;
        ss(root.right, snap);
    }
}
