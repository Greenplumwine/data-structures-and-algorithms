package main.datastructures.binarytree.implementation;

import java.util.LinkedList;
import java.util.Queue;

/**
 * 二叉搜索树的实现
 *
 * @author jingzepei
 * @version 1.0
 * @since 2022-11-27 17:32
 */
public class BinaryTree<T> {

    /**
     * 二叉搜索树的节点
     *
     * @param <T>
     */
    public class Node<T> {
        /**
         * 节点的值
         */
        T value;
        /**
         * 当前节点的左子节点
         */
        Node<T> left;
        /**
         * 当前节点的右子节点
         */
        Node<T> right;

        /**
         * 当前节点的双亲节点
         */
        Node<T> parent;

        public Node(T value) {
            this.value = value;
        }
    }

    /**
     * 当前树的根节点
     */
    private Node<T> root;


    /**
     * 插入值
     *
     * @param data
     */
    public void insert(T data) {
        if (root == null) {
            this.root = new Node<>(data);
            return;
        }

        Node<T> node = root;
        while (node != null) {
            if (data.equals(node.value)) {
                break;
            }

            if (node.value.hashCode() > data.hashCode()) {
                if (node.left == null) {
                    node.left = new Node<>(data);
                    node.left.parent = node;
                    break;
                }
                node = node.left;
            } else {
                if (node.right == null) {
                    node.right = new Node<>(data);
                    node.right.parent = node;
                    break;
                }
                node = node.right;
            }
        }
    }


    /**
     * 前序遍历
     */
    public void presequenceTraversal(Node<T> node) {
        if (node != null) {
            System.out.println(node.value);
            this.presequenceTraversal(node.left);
            this.presequenceTraversal(node.right);
        }
    }

    /**
     * 中序遍历
     */
    public void middleOrderTraversal(Node<T> node) {
        if (node != null) {
            this.presequenceTraversal(node.left);
            System.out.println(node.value);
            this.presequenceTraversal(node.right);
        }
    }

    /**
     * 后序遍历
     */
    public void postOrderTraversal(Node<T> node) {
        if (node != null) {
            this.presequenceTraversal(node.left);
            this.presequenceTraversal(node.right);
            System.out.println(node.value);
        }
    }

    /**
     * 层次遍历
     *
     * @param
     */
    public void hierarchicalTraversal() {
        if (this.root == null) {
            return;
        }

        Queue<Node<T>> snap = new LinkedList<>();
        snap.add(root.left);
        snap.add(root.right);

        while (!snap.isEmpty()) {
            Node<T> node = snap.poll();
            System.out.println(node.value);
            if (node.left != null) {
                snap.add(node.left);
            }
            if (node.right != null) {
                snap.add(node.right);
            }
        }
    }


    /**
     * 查找
     *
     * @param data
     * @return
     */
    public Boolean find(T data) {
        return this.find(this.root, data);
    }

    private Boolean find(Node<T> root, T data) {
        if (root == null) {
            return false;
        }

        int nodeHashCode = root.hashCode();

        if (data.equals(root.value)) {
            return true;
        }

        if (nodeHashCode > data.hashCode()) {
            return this.find(root.left, data);
        } else {
            return this.find(root.right, data);
        }
    }

    /**
     * 获取最大节点
     *
     * @return
     */
    public Node<T> maximum(Node<T> node) {
        if (node == null) {
            return null;
        }

        while (node.right != null) {
            node = node.right;
        }

        return node;
    }

    /**
     * 获取最小节点
     *
     * @return
     */
    public Node<T> minimum(Node<T> node) {
        if (node == null) {
            return null;
        }

        while (node.left != null) {
            node = node.left;
        }

        return node;
    }


    /**
     * 查找前驱节点
     * 节点的前驱: 是该节点的左子树中的最大节点
     * @param data
     * @return
     */
    public Node<T> precursorNode(Node<T> data) {
        // 如果x存在左孩子，则"x的前驱结点"为 "以其左孩子为根的子树的最大结点"。
        if (data.left != null) {
            return this.maximum(data);
        }

        // 如果x没有左孩子。则x有以下两种可能:
        // (01) x是"一个右孩子"，则"x的前驱结点"为 "它的父结点"。
        // (02) x是"一个左孩子"，则查找"x的最低的父结点，并且该父结点要具有右孩子"，找到的这个"最低的父结点"就是"x的前驱结点"。
        Node<T> snap = data.parent;
        while ((snap != null) && (data == snap.left)) {
            data = snap;
            snap = snap.parent;
        }

        return snap;
    }


    /**
     * 查找后继节点
     * 节点的后继: 是该节点的右子树中的最小节点。
     * @param data
     * @return
     */
    public Node<T> successorNode(Node<T> data) {
        // 如果存在右子树时，则"x的后继结点"为 "以其右孩子为根的子树的最小结点"。
        if (data.right != null) {
            return this.minimum(data);
        }

        // 如果x没有右孩子。则x有以下两种可能:
        // (01) x是"一个左孩子"，则"x的后继结点"为 "它的父结点"。
        // (02) x是"一个右孩子"，则查找"x的最低的父结点，并且该父结点要具有左孩子"，
        // 找到的这个"最低的父结点"就是"x的后继结点"。
        Node<T> snap = data.parent;

        while ((snap != null) && (data == snap.right)) {
            data = snap;
            snap = snap.parent;
        }

        return snap;
    }

    /**
     * 删除节点
     *
     * @param data
     * @return
     */
    public Node<T> remove(T data) {
        // todo 还需要在学习
        Node<T> node = root;
        int hashCode = data.hashCode();
        while (node != null) {
            if (data.equals(node.value)) {
                Node<T> x = null, y = null;
                if ((node.left == null) || (node.right == null)) {
                    y = node;
                } else {
                    y = successorNode(node);
                }

                if (y.left != null) {
                    x = y.left;
                } else {
                    x = y.right;
                }

                if (x != null) {
                    x.parent = y.parent;
                }

                if (y.parent == null) {
                    this.root = x;
                } else if (y == y.parent.left) {
                    y.parent.left = x;
                } else {
                    y.parent.right = x;
                }

                if (y != node) {
                    node.value = y.value;
                }

                return y;
            }

            if (node.value.hashCode() > hashCode) {
                node = node.left;
            } else {
                node = node.right;
            }
        }

        return node;
    }
}
