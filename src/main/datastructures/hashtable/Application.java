package main.datastructures.hashtable;

import java.util.*;

/**
 * 哈希表数据结构算法
 *
 * @author jingzepei
 * @version 1.0
 * @since 2022/November/13 17:39
 */
public class Application {
    public static void main(String[] args) {
        // 两数之和
        // int[] nums = new int[] {2,7,11,15};
        // int[] result = twoSum(nums, 9);

        // int[] nums = new int[]{1, 2, 3, 1};
        // System.out.println(containsDuplicate(nums));

        int[] nums = new int[]{100, 4, 200, 1, 3, 2};
        System.out.println(longestConsecutive(nums));
    }


    /**
     * <p>题目：两数之和</p>
     * <ul>
     *     <li>来源：Leet Code </li>
     *     <li>题号：1</li>
     *     <li>难度: <span style="color:green">简单</span></li>
     *     <li>给定一个整数数组 nums和一个整数目标值 target，请你在该数组中找出 和为目标值 target 的那两个整数，并返回它们的数组下标。
     *          你可以假设每种输入只会对应一个答案。但是，数组中同一个元素在答案里不能重复出现。
     *          你可以按任意顺序返回答案。</li>
     *     <li>
     *         <p>提示：</p>
     *         <ul>
     * 	            <li> 2  <= nums.length  <= 10<sup>4</sup> </li>
     * 	            <li> -10<sup>9</sup>  <= nums[i]  <= 10<sup>9</sup> </li>
     * 	            <li> -10<sup>9</sup>  <= target  <= 10<sup>9</sup> </li>
     * 	            <li><strong>只会存在一个有效答案</strong></li>
     *         </ul>
     *     </li>
     *     <li>进阶：你可以想出一个时间复杂度小于 O(n<sup>2</sup>) 的算法吗？</li>
     * </ul>
     *
     * @param nums   整数数组
     * @param target 两数之和的目标值
     */
    public static int[] twoSum(int[] nums, int target) {
        Map<Integer, Integer> snap = new HashMap<>(nums.length);

        for (int i = 0; i < nums.length; i++) {
            int res = target - nums[i];

            if (!snap.containsKey(res)) {
                snap.put(nums[i], i);
            } else {
                return new int[]{snap.get(res), i};
            }
        }

        return new int[0];
    }


    /**
     * <p>题目：存在重复元素</p>
     * <ul>
     *     <li>来源：Leet Code </li>
     *     <li>题号：217</li>
     *     <li>难度: <span style="color:green">简单</span></li>
     *     <li>
     *         给你一个整数数组 nums 。如果任一值在数组中出现 至少两次 ，返回 true ；如果数组中每个元素互不相同，返回 false 。
     *     </li>
     *     <li>
     *         <p>提示：</p>
     *         <ul>
     * 	            <li> 1  <= nums.length  <= 10<sup>5</sup> </li>
     * 	            <li> -10<sup>9</sup>  <= nums[i]  <= 10<sup>9</sup> </li>
     *         </ul>
     *     </li>
     * </ul>
     *
     * @param nums 整数数组
     */
    public static boolean containsDuplicate(int[] nums) {
        Set<Integer> snap = new HashSet<>();

        for (int num : nums) {
            if (snap.contains(num)) {
                return true;
            } else {
                snap.add(num);
            }
        }

        return false;
    }


    /**
     * <p>题目：最长和谐子序列</p>
     * <ul>
     *     <li>来源：Leet Code </li>
     *     <li>题号：594</li>
     *     <li>难度: <span style="color:green">简单</span></li>
     *     <li>
     *          和谐数组是指一个数组里元素的最大值和最小值之间的差别 正好是 1 。
     *          现在，给你一个整数数组 nums ，请你在所有可能的子序列中找到最长的和谐子序列的长度。
     *          数组的子序列是一个由数组派生出来的序列，它可以通过删除一些元素或不删除元素、且不改变其余元素的顺序而得到。
     *     </li>
     *     <li>
     *         <p>提示：</p>
     *         <ul>
     * 	            <li> 1  <= nums.length  <= 2 * 10<sup>4</sup> </li>
     * 	            <li> -10<sup>9</sup>  <= nums[i]  <= 10<sup>9</sup> </li>
     *         </ul>
     *     </li>
     * </ul>
     *
     * @param nums 整数数组
     */
    public int findLHS(int[] nums) {

        // 大神实现，自己没有考虑到这个方法
        // 将数组变为有序，如果有序，则之差等于1的数，就连接在了一起，然后用双指针，左侧指针指向最小值的开始位，右侧指向最大值开始位
        // 两个位置下相减 + 1就能得出数量
        Arrays.sort(nums);
        int begin = 0, max = 0;
        for (int i = 0; i < nums.length; i++) {
            if (nums[i] - nums[begin] > 1) {
                begin++;
            }

            if (nums[i] - nums[begin] == 1) {
                max = Math.max(max, i - begin + 1);
            }
        }

        // 自己实现的办法
        // Map<Integer, Integer> snap = new HashMap<>();
        //
        // for (int num : nums) {
        //     snap.put(num, snap.getOrDefault(num, 0) + 1);
        // }
        //
        //
        // int max = 0;
        // for (Integer num : snap.keySet()) {
        //     int newNum = num + 1;
        //     if (snap.containsKey(newNum)) {
        //         max = Math.max(max, snap.get(newNum) + snap.get(num));
        //     }
        // }

        return max;
    }


    /**
     * <p>题目：最长连续序列</p>
     * <ul>
     *     <li>来源：leet code </li>
     *     <li>题号：128</li>
     *     <li>难度: <span style="color:orange">中等</span></li>
     *     <li>
     *          给定一个未排序的整数数组 nums ，找出数字连续的最长序列（不要求序列元素在原数组中连续）的长度。
     *          请你设计并实现时间复杂度为o(n) 的算法解决此问题。
     *     </li>
     *     <li>
     *         <p>提示：</p>
     *         <ul>
     * 	            <li> 0  <= nums.length  <= 10<sup>5</sup> </li>
     * 	            <li> -10<sup>9</sup>  <= nums[i]  <= 10<sup>9</sup> </li>
     *         </ul>
     *     </li>
     * </ul>
     *
     * @param nums 整数数组
     */
    public static int longestConsecutive(int[] nums) {

        // 自己想出来的
        Arrays.sort(nums);
        int cur = 0, max = 0;
        for (int end = 1; end < nums.length; end++) {
            // 这个题比较扯淡，题目没有说清楚，重复的元素不算
            // 在两个数对比过程中，如果出现重复元素，则认为是一个
            if (nums[end] == nums[end - 1]) {
                continue;
            }
            if (nums[end] - nums[end - 1] == 1) {
                cur++;
            } else {
                // 为什么要 cur + 1，下标是从1开始的，会忽略起始值，因此需要 + 1
                max = Math.max(cur + 1, max);
                cur = 0;
            }
        }
        // 为什么要 cur + 1，下标是从1开始的，会忽略起始值，因此需要 + 1
        max = Math.max(cur + 1, max);
        return max;

        // if (nums == null) {
        //     return 0;
        // }
        //
        // // 使用哈希表的算法
        // HashSet<Integer> snap = new HashSet<>();
        //
        // for (int num : nums) {
        //     snap.add(num);
        // }
        //
        // int result = 0;
        // for (Integer num : snap) {
        //     int cur = num;
        //     if (!snap.contains(num - 1)) {
        //         while (snap.contains(cur + 1)) {
        //             cur ++;
        //         }
        //     }
        //
        //     result = Math.max(result, cur - num + 1);
        // }
        //
        // return result;
    }
}
