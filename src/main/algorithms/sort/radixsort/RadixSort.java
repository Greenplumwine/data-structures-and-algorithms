package main.algorithms.sort.radixsort;

import java.util.StringJoiner;

/**
 * 基数排序
 *
 * @author jingzepei
 * @version 1.0
 * @since 2022/12/25 14:49
 */
public class RadixSort {
    public static void main(String[] args) {
        int[] data = new int[]{-1, 2, -8, 10};
        StringJoiner joiner = new StringJoiner("，");

        for (int datum : data) {
            joiner.add(String.valueOf(datum));
        }

        System.out.printf("原始数组数据为: %s%n", joiner);

        RadixSort sort = new RadixSort();

        data = sort.radixSorting(data);

        joiner = new StringJoiner("，");

        for (int datum : data) {
            joiner.add(String.valueOf(datum));
        }

        System.out.printf("排序后数组数据为: %s%n", joiner);
    }


    /**
     * 基数排序
     * 原理：将整数按位切割成不同的数字，然后分别按位比较，数位较短的前面补0
     * 执行用时：15 ms, 在所有 Java 提交中击败了94.93%的用户
     * 内存消耗：50.8 MB, 在所有 Java 提交中击败了83.33%的用户
     * @param nums  待排序的数组
     * @return 排序后的数组
     */
    public int[] radixSorting(int[] nums) {
        if (null == nums || nums.length < 2) {
            return nums;
        }
        int length = nums.length;
        // 找出最大值，计算位数
        int max = Integer.MIN_VALUE, min = Integer.MAX_VALUE;

        for (int num : nums) {
            max = Math.max(num, max);
            min = Math.min(num, min);
        }
        // 避免负数，让当前数组从0开始
        for (int i = 0; i < length; i++) {
            nums[i] -= min;
        }
        max -= min;

        // 计算最大的数有多少位
        int numberOfDigits = 1;
        while (max >= 10) {
            max /= 10;
            ++numberOfDigits;
        }
        // 计数桶
        int[] buckets = new int[10];
        // 基数
        int cardinality = 1;

        for (int i = 1; i <= numberOfDigits; i++) {
            int[] temp = new int[length];

            // 计数
            for (int num : nums) {
                buckets[(num / cardinality) % 10]++;
            }

            for (int k = 1; k < 10; k++) {
                buckets[k] = buckets[k - 1] + buckets[k];
            }

            for (int q = length - 1; q >= 0; q--) {
                int k = (nums[q] / cardinality) % 10;
                temp[buckets[k] - 1] = nums[q];
                buckets[k]--;
            }

            // 复制临时数组到nums
            System.arraycopy(temp, 0, nums,0, nums.length);

            // 每次循环都乘以10，用于取位数
            cardinality *= 10;
            // 清空计数桶
            buckets = new int[10];
        }

        for(int i = 0; i < length; i++){
            nums[i] += min;
        }

        return nums;
    }
}
