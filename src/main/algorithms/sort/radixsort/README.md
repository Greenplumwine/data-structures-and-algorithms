# 排序算法 ---- 基数排序
编写时间: 2022-12-25 16:35
> 参考来源: 
> 1. https://zhuanlan.zhihu.com/p/126116878
> 2. https://pdai.tech/md/algorithm/alg-sort-x-radix.html
## 介绍
基数排序（Radix sort）是一种非比较型整数排序算法。
### 原理
原理是将整数按位数切割成不同的数字，然后按每个位数分别比较。基数排序的方式可以采用```LSD（Least significant digital）```或```MSD（Most significant digital）```，**LSD的排序方式由键值的最右边开始，而MSD则相反，由键值的最左边开始**。
- MSD：先从高位开始进行排序，在每个关键字上，可采用计数排序
- LSD: 先从低位开始进行排序，在每个关键字上，可采用桶排序

## 实现
### 图文实现
设有数组 array = {53, 3, 542, 748, 14, 214, 154, 63, 616}，对其进行基数排序：
![img.png](img.png)
在上图中，首先将所有待比较数字统一为统一位数长度，接着从最低位开始，依次进行排序。
1. 按照个位数进行排序。
2. 按照十位数进行排序。
3. 按照百位数进行排序。  

排序后，数列就变成了一个有序序列。
### 实现逻辑
1. 遍历数组，获取到最大的数，计算出有多少位
2. 从最低位开始，依次进行排序。
3. 这样从最低位排序一直到最高位排序完成以后, 数列就变成一个有序序列。
### 代码实现
```java
/**
 * 基数排序
 *
 * @author jingzepei
 * @version 1.0
 * @since 2022/12/25 14:49
 */
public class RadixSort {
    public static void main(String[] args) {
        int[] data = new int[]{-1, 2, -8, 10};
        StringJoiner joiner = new StringJoiner("，");

        for (int datum : data) {
            joiner.add(String.valueOf(datum));
        }

        System.out.printf("原始数组数据为: %s%n", joiner);

        RadixSort sort = new RadixSort();

        data = sort.radixSorting(data);

        joiner = new StringJoiner("，");

        for (int datum : data) {
            joiner.add(String.valueOf(datum));
        }

        System.out.printf("排序后数组数据为: %s%n", joiner);
    }


    /**
     * 基数排序
     * 原理：将整数按位切割成不同的数字，然后分别按位比较，数位较短的前面补0
     * 执行用时：15 ms, 在所有 Java 提交中击败了94.93%的用户
     * 内存消耗：50.8 MB, 在所有 Java 提交中击败了83.33%的用户
     * @param nums  待排序的数组
     * @return 排序后的数组
     */
    public int[] radixSorting(int[] nums) {
        if (null == nums || nums.length < 2) {
            return nums;
        }
        int length = nums.length;
        // 找出最大值，计算位数
        int max = Integer.MIN_VALUE, min = Integer.MAX_VALUE;

        for (int num : nums) {
            max = Math.max(num, max);
            min = Math.min(num, min);
        }
        // 避免负数，让当前数组从0开始
        for (int i = 0; i < length; i++) {
            nums[i] -= min;
        }
        max -= min;

        // 计算最大的数有多少位
        int numberOfDigits = 1;
        while (max >= 10) {
            max /= 10;
            ++numberOfDigits;
        }
        // 计数桶
        int[] buckets = new int[10];
        // 基数
        int cardinality = 1;

        for (int i = 1; i <= numberOfDigits; i++) {
            int[] temp = new int[length];

            // 计数
            for (int num : nums) {
                buckets[(num / cardinality) % 10]++;
            }

            for (int k = 1; k < 10; k++) {
                buckets[k] = buckets[k - 1] + buckets[k];
            }

            for (int q = length - 1; q >= 0; q--) {
                int k = (nums[q] / cardinality) % 10;
                temp[buckets[k] - 1] = nums[q];
                buckets[k]--;
            }

            // 复制临时数组到nums
            System.arraycopy(temp, 0, nums,0, nums.length);

            // 每次循环都乘以10，用于取位数
            cardinality *= 10;
            // 清空计数桶
            buckets = new int[10];
        }

        for(int i = 0; i < length; i++){
            nums[i] += min;
        }

        return nums;
    }
}
```
## 复杂度和稳定性
### 复杂度
> 时间复杂度：O(k × N)
> 空间复杂度: O(k + N)  

设待排序的数组R[1..n]，数组中最大的数是d位数，基数为r（如基数为10，即10进制，最大有10种可能，即最多需要10个桶来映射数组元素）。

处理一位数，需要将数组元素映射到r个桶中，映射完成后还需要收集，相当于遍历数组一遍，最多元素数为n，则时间复杂度为O(n+r)。所以，总的时间复杂度为O(d*(n+r))。

基数排序过程中，用到一个计数器数组，长度为r，还用到一个rn的二位数组来做为桶，所以空间复杂度为O(rn)。
### 稳定性
> 稳定性：稳定  

基数排序基于分别排序，分别收集，所以是稳定的。

## 总结
基数排序与计数排序、桶排序这三种排序算法都利用了桶的概念，但对桶的使用方法上有明显差异：

- 基数排序：根据键值的每位数字来分配桶；
- 计数排序：每个桶只存储单一键值；
- 桶排序：每个桶存储一定范围的数值；  

基数排序不是直接根据元素整体的大小进行元素比较，而是将原始列表元素分成多个部分，对每一部分按一定的规则进行排序，进而形成最终的有序列表。


