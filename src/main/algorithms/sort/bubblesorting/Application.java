package main.algorithms.sort.bubblesorting;

import java.util.Random;
import java.util.StringJoiner;

/**
 * 冒泡排序算法学习
 *
 * @author jingzepei
 * @version 1.0
 * @since 2022-12-07 12:51
 */
public class Application {
    public static void main(String[] args) {
        int[] data = arrayGeneration(10);

        StringJoiner joiner = new StringJoiner("，");

        for (int datum : data) {
            joiner.add(String.valueOf(datum));
        }

        System.out.printf("原始数组数据为: %s%n", joiner);

        Application application = new Application();

        application.bubbleSort(data);

        joiner = new StringJoiner("，");

        for (int datum : data) {
            joiner.add(String.valueOf(datum));
        }

        System.out.printf("排序后数组数据为: %s%n", joiner);
    }

    /**
     * 生成随机大小数组
     *
     * @param count 数组长度
     *
     * @return 返回一个指定长度随机生成的数组
     */
    public static int[] arrayGeneration(int count) {
        int[] res = new int[count];
        Random random = new Random();
        for (int i = 0; i < count; i++) {
            res[i] = random.nextInt();
        }

        return res;
    }

    /**
     * 冒泡排序具体实现
     * 时间复杂度: O(n<sup>2</sup>)
     * PS: 在leetcode 912题，时间超时
     * 空间复杂度：O(1)
     * @param src 需要排序的数组
     */
    public void bubbleSort(int[] src) {
        if (src == null || src.length < 1) {
            return;
        }

        int length = src.length;

        for (int i = length - 1; i > 0; i--) {
            boolean swap = false;
            for (int j = 0; j < i; j++) {
                if (src[j] > src[j + 1]) {
                    int tmp = src[j];
                    src[j] = src[j + 1];
                    src[j + 1] = tmp;
                    swap = true;
                }
            }

            // 当swap值为false的时候，说明数组已经有序，不需要在继续进行
            if (!swap) {
                break;
            }
        }
    }
}
