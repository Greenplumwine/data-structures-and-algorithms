package main.algorithms.sort.quicksort;

import java.util.Random;
import java.util.StringJoiner;

/**
 * 快速排序实现
 *
 * @author jingzepei
 * @version 1.0
 * @since 2022-12-07 19:47
 */
public class QuickSort {
    public static void main(String[] args) {
        int[] array = arrayGeneration(10);

        StringJoiner joiner = new StringJoiner("，");

        for (int datum : array) {
            joiner.add(String.valueOf(datum));
        }

        System.out.printf("原始数组数据为: %s%n", joiner);


        QuickSort quickSort = new QuickSort();
        quickSort.quickSorting(array, 0, array.length - 1);

        joiner = new StringJoiner("，");

        for (int datum : array) {
            joiner.add(String.valueOf(datum));
        }

        System.out.printf("排序后数组数据为: %s%n", joiner);
    }

    /**
     * 生成随机大小数组
     *
     * @param count 数组长度
     *
     * @return 返回一个指定长度随机生成的数组
     */
    public static int[] arrayGeneration(int count) {
        int[] res = new int[count];
        Random random = new Random();
        for (int i = 0; i < count; i++) {
            res[i] = random.nextInt();
        }

        return res;
    }

    /**
     * 快速排序具体实现
     * 快速排序采用了分治思想，双指针思想
     *
     * @param src   需要排序的数组
     * @param start 数组左边界
     * @param end   数组右边界
     */
    public void quickSorting(int[] src, int start, int end) {
        if (start >= end) {
            return;
        }

        int left = start, right = end, benchmark = src[left];

        while (left < right) {
            // 先遍历右边，找到第一个比基准值小的值
            while (left < right && src[right] > benchmark) {
                right--;
            }

            if (left < right) {
                src[left++] = src[right];
            }

            // 在遍历左遍，找到第一个比基准值大的值
            while (left < right && src[left] < benchmark) {
                left++;
            }

            if (left < right) {
                src[right--] = src[left];
            }
        }
        // 完成后将剩余的位置放入基准值即可
        src[left] = benchmark;
        /*
         *  将两部分的子集分别排序
         */
        this.quickSorting(src, start, left - 1);
        this.quickSorting(src, right + 1, end);
    }
}
