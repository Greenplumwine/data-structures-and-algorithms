package main.algorithms.sort.selectionsort;

import java.util.Random;
import java.util.StringJoiner;

/**
 * 选择排序实现
 *
 * @author jingzepei
 * @version 1.0
 * @since 2022/12/15 17:58
 */
public class SelectionSort {
    public static void main(String[] args) {
        int[] data = arrayGeneration(10);
        // int[] data = new int[] {10, 5, 3, 4, 1, 2, 6, 0};
        StringJoiner joiner = new StringJoiner("，");

        for (int datum : data) {
            joiner.add(String.valueOf(datum));
        }

        System.out.printf("原始数组数据为: %s%n", joiner);

        SelectionSort sort = new SelectionSort();

        data = sort.selectionSorting(data);

        joiner = new StringJoiner("，");

        for (int datum : data) {
            joiner.add(String.valueOf(datum));
        }

        System.out.printf("排序后数组数据为: %s%n", joiner);
    }

    /**
     * 生成随机大小数组
     *
     * @param count 数组长度
     *
     * @return 返回一个指定长度随机生成的数组
     */
    public static int[] arrayGeneration(int count) {
        int[] res = new int[count];
        Random random = new Random();
        for (int i = 0; i < count; i++) {
            res[i] = random.nextInt(100);
        }

        return res;
    }

    /**
     * 选择排序具体实现
     * 基本思路就是，每次遍历，找出最小的值，然后将当前的数和最小的数交换一下位置
     * 在leetcode 912题上超出时间范围
     * @param nums  需要排序的数
     * @return
     */
    public int[] selectionSorting(int[] nums) {
        if (nums == null || nums.length < 2) {
            return nums;
        }

        for (int i = 0; i < nums.length - 1; i++) {
            int minIndex = i;
            for (int j = i + 1; j < nums.length; j++) {
                if (nums[minIndex] > nums[j]) {
                    minIndex = j;
                }
            }

            if (minIndex != i) {
                int temp = nums[i];
                nums[i] = nums[minIndex];
                nums[minIndex] = temp;
            }
        }

        return nums;
    }
}
