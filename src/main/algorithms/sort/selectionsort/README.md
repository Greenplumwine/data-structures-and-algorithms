# 排序算法 ---- 选择排序(Selection sort)
编写时间: 2022-12-15 18:00
## 基本思想
首先在未排序的数组中找到最小(最大)的元素，然后将其存放在数列的起始位置；接着，再从剩余未排序的元素中继续寻找最小(最大)的元素，然后放到已排序的序列末尾，以此类推，直到所有元素均排序完成

## 选择排序实现
下面以数列``{20,40,30,10,60,50}``为例，演示它的选择排序过程(如下图)。
![img.png](img.png)

### 排序流程：
- 第1趟: i=0。找出a[1...5]中的最小值a[3]=10，然后将a[0]和a[3]互换。 数列变化: 20,40,30,10,60,50 -- > 10,40,30,20,60,50
- 第2趟: i=1。找出a[2...5]中的最小值a[3]=20，然后将a[1]和a[3]互换。 数列变化: 10,40,30,20,60,50 -- > 10,20,30,40,60,50
- 第3趟: i=2。找出a[3...5]中的最小值，由于该最小值大于a[2]，该趟不做任何处理。
- 第4趟: i=3。找出a[4...5]中的最小值，由于该最小值大于a[3]，该趟不做任何处理。
- 第5趟: i=4。交换a[4]和a[5]的数据。 数列变化: 10,20,30,40,60,50 -- > 10,20,30,40,50,60

### 代码实现
```java
public class SelectionSort {
    public static void main(String[] args) {
        int[] data = arrayGeneration(10);
        StringJoiner joiner = new StringJoiner("，");

        for (int datum : data) {
            joiner.add(String.valueOf(datum));
        }

        System.out.printf("原始数组数据为: %s%n", joiner);

        SelectionSort sort = new SelectionSort();

        data = sort.selectionSorting(data);

        joiner = new StringJoiner("，");

        for (int datum : data) {
            joiner.add(String.valueOf(datum));
        }

        System.out.printf("排序后数组数据为: %s%n", joiner);
    }

    /**
     * 生成随机大小数组
     *
     * @param count 数组长度
     *
     * @return 返回一个指定长度随机生成的数组
     */
    public static int[] arrayGeneration(int count) {
        int[] res = new int[count];
        Random random = new Random();
        for (int i = 0; i < count; i++) {
            res[i] = random.nextInt(100);
        }

        return res;
    }

    public int[] selectionSorting(int[] nums) {
        if (nums == null || nums.length < 2) {
            return nums;
        }

        for (int i = 0; i < nums.length - 1; i++) {
            int minIndex = i;
            for (int j = i + 1; j < nums.length; j++) {
                if (nums[minIndex] > nums[j]) {
                    minIndex = j;
                }
            }

            if (minIndex != i) {
                int temp = nums[i];
                nums[i] = nums[minIndex];
                nums[minIndex] = temp;
            }
        }

        return nums;
    }
}
```

## 复杂度和稳定性
### 时间复杂度
> 选择排序的时间复杂度为O(n<sup>2</sup>)
### 空间复杂度
> 选择排序的空间复杂度为O(1)  

为什么是O(1)？因为没有引入数组等数据，只使用了常亮进行临时存储
### 算法稳定性
> 选择排序的算法稳定性为**不稳定**

