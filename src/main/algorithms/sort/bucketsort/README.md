# 排序算法 ---- 桶排序
编写时间: 2022-12-24 15:07

## 介绍
桶排序(Bucket Sort)的原理很简单，将数组根据一定的规则尽可能平均分到有限数量的桶子里。每个桶在分别个别排序（有可能再使用别的排序算法或是以递归方式继续使用桶排序进行排序）

## 实现
### 图文实现
假设a={8,2,3,4,3,6,6,3,9}, max=10。此时，将数组a的所有数据都放到需要为0-9的桶中。如下图:
![img.png](img.png)
在将数据放到桶中之后，再通过一定的算法，将桶中的数据提出出来并转换成有序数组。就得到我们想要的结果了。
### 代码实现
```java
/**
 * 桶排序实现
 *
 * @author jingzepei
 * @version 1.0
 * @since 2022/12/16 15:16
 */
public class BucketSort {
    public static void main(String[] args) {
        int[] data = new int[]{10, 5, 3, 4, 1, 2, 6, 0};
        StringJoiner joiner = new StringJoiner("，");

        for (int datum : data) {
            joiner.add(String.valueOf(datum));
        }

        System.out.printf("原始数组数据为: %s%n", joiner);

        BucketSort sort = new BucketSort();

        data = sort.bucketSort(data);

        joiner = new StringJoiner("，");

        for (int datum : data) {
            joiner.add(String.valueOf(datum));
        }

        System.out.printf("排序后数组数据为: %s%n", joiner);
    }

    /**
     * 桶排序具体实现
     * 需要将一个待排序的数组中所有元素，按照某个规则，平均分配到N个桶中，然后每个桶之间的元素相互排序
     * leetcode 912算法题 执行用时:29ms, 超过66.83%，内存消耗：51.2mb，超过70.8%
     * @param nums 待排序的数组
     *
     * @return  排序后的数组
     */
    public int[] bucketSort(int[] nums) {
        if (nums == null || nums.length < 2) {
            return nums;
        }

        int min = nums[0], max = nums[nums.length - 1];

        // 找出最大和最小的数
        for (int num : nums) {
            min = Math.min(min, num);
            max = Math.max(max, num);
        }

        // 初始化桶
        // 每个桶的元素数量
        int bucketItemCount = 5;
        // 桶的个数
        int bucketLength = (max - min) / bucketItemCount + 1;
        List<List<Integer>> buckets = new ArrayList<>();

        for (int i = 0; i < bucketLength; i++) {
            buckets.add(new ArrayList<Integer>());
        }

        // 分桶
        for (int num : nums) {
            int index = (num - min) / bucketItemCount;
            buckets.get(index).add(num);
        }

        // 桶内排序
        for (List<Integer> bucket : buckets) {
            this.sort(bucket, bucket.size() / 2);
        }


        // 取值
        int k = 0;
        for (List<Integer> bucket : buckets) {
            for (Integer integer : bucket) {
                nums[k++] = integer;
            }
        }

        return nums;
    }


    /**
     * 桶内的排序可以选择其他排序算法进行排序，此处使用的是shell排序
     * @param bucket 待排序的桶
     * @param gap   步长
     */
    private void sort(List<Integer> bucket, int gap) {
        if (gap < 1) {
            return;
        }

        for (int i = 0; i < gap; i++) {
            for (int j = i + gap; j < bucket.size(); j += gap) {
                if (bucket.get(j) < bucket.get(j - gap)) {
                    int temp = bucket.get(j), middle = j - gap;

                    for (; middle >= 0 && temp < bucket.get(middle); middle -= gap) {
                        bucket.set(middle + gap, bucket.get(middle));
                    }

                    bucket.set(middle + gap,temp);
                }
            }
        }

        this.sort(bucket, gap / 2);
    }
}

```

## 复杂度和稳定性
### 复杂度
#### 复杂度
- 时间复杂度：O(n + k)，k为桶数量，这里要说明下，因为单个桶的大小bucketSize很小（我这里设置为5），因此对单个桶的排序的时间复杂度是常数级别的，可以忽略；从时间复杂度也可以看出，为什么分了桶的插入排序不仅不会超时，还比较快了。
- 空间复杂度：O(n + k)，桶的数量为k，里面放的数总共为n个，空桶也占空间所以总共为O(n + k)
#### 稳定性
稳定