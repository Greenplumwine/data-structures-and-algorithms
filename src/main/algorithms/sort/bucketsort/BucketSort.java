package main.algorithms.sort.bucketsort;

import java.util.ArrayList;
import java.util.List;
import java.util.StringJoiner;

/**
 * 桶排序实现
 *
 * @author jingzepei
 * @version 1.0
 * @since 2022/12/16 15:16
 */
public class BucketSort {
    public static void main(String[] args) {
        int[] data = new int[]{10, 5, 3, 4, 1, 2, 6, 0};
        StringJoiner joiner = new StringJoiner("，");

        for (int datum : data) {
            joiner.add(String.valueOf(datum));
        }

        System.out.printf("原始数组数据为: %s%n", joiner);

        BucketSort sort = new BucketSort();

        data = sort.bucketSort(data);

        joiner = new StringJoiner("，");

        for (int datum : data) {
            joiner.add(String.valueOf(datum));
        }

        System.out.printf("排序后数组数据为: %s%n", joiner);
    }

    /**
     * 桶排序具体实现
     * 需要将一个待排序的数组中所有元素，按照某个规则，平均分配到N个桶中，然后每个桶之间的元素相互排序
     * leetcode 912算法题 执行用时:29ms, 超过66.83%，内存消耗：51.2mb，超过70.8%
     * @param nums 待排序的数组
     *
     * @return  排序后的数组
     */
    public int[] bucketSort(int[] nums) {
        if (nums == null || nums.length < 2) {
            return nums;
        }

        int min = nums[0], max = nums[nums.length - 1];

        // 找出最大和最小的数
        for (int num : nums) {
            min = Math.min(min, num);
            max = Math.max(max, num);
        }

        // 初始化桶
        int bucketLength = (max - min) / 5 + 1;
        List<List<Integer>> buckets = new ArrayList<>();

        for (int i = 0; i < bucketLength; i++) {
            buckets.add(new ArrayList<>());
        }

        // 分桶
        for (int num : nums) {
            int index = (num - min) / 5;
            buckets.get(index).add(num);
        }

        // 桶内排序
        for (List<Integer> bucket : buckets) {
            this.sort(bucket, bucket.size() / 2);
        }


        // 取值
        int k = 0;
        for (List<Integer> bucket : buckets) {
            for (Integer integer : bucket) {
                nums[k++] = integer;
            }
        }

        return nums;
    }


    /**
     * 桶内的排序可以选择其他排序算法进行排序，此处使用的是shell排序
     * @param bucket    待排序的桶
     * @param gap       步长
     */
    private void sort(List<Integer> bucket, int gap) {
        if (gap < 1) {
            return;
        }

        for (int i = 0; i < gap; i++) {
            for (int j = i + gap; j < bucket.size(); j += gap) {
                if (bucket.get(j) < bucket.get(j - gap)) {
                    int temp = bucket.get(j), middle = j - gap;

                    for (; middle >= 0 && temp < bucket.get(middle); middle -= gap) {
                        bucket.set(middle + gap, bucket.get(middle));
                    }

                    bucket.set(middle + gap,temp);
                }
            }
        }

        this.sort(bucket, gap / 2);
    }
}
