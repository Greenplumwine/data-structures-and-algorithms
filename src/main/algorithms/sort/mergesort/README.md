# 排序算法 ---- 归并排序
编写时间: 2022-12-24 15:59

## 介绍
归并排序，是创建在归并操作上的一种有效的排序算法。算法是采用**分治法**（Divide and Conquer）的一个非常典型的应用，且各层分治递归可以同时进行。归并排序思路简单，速度仅次于快速排序，为稳定排序算法，一般用于对总体无序，但是各子项相对有序的数列。
## 基本思想
归并排序是用**分治思想**，分治模式在每一层递归上有三个步骤：

- 分解（Divide）：将n个元素分成个含 ```n÷2``` 个元素的子序列。
- 解决（Conquer）：用合并排序法对两个子序列递归的排序。
- 合并（Combine）：合并两个已排序的子序列已得到排序结果。

## 实现
### 图文实现
![归并排序动画演示](https://pic4.zhimg.com/v2-a29c0dd0186d1f8cef3c5ebdedf3e5a3_b.webp)

具体的我们以一组无序数列｛14，12，15，13，11，16｝为例分解说明，如下图所示：
![img.png](img.png)

### 代码实现
```java
package main.algorithms.sort.mergesort;

import java.util.Random;
import java.util.StringJoiner;

/**
 * 归并排序具体实现
 *
 * @author jingzepei
 * @version 1.0
 * @since 2022/12/24 15:27
 */
public class MergeSort {
    public static void main(String[] args) {
        int[] array = arrayGeneration(10);

        StringJoiner joiner = new StringJoiner("，");

        for (int datum : array) {
            joiner.add(String.valueOf(datum));
        }

        System.out.printf("原始数组数据为: %s%n", joiner);


        MergeSort mergeSort = new MergeSort();
        mergeSort.mergeSorting(array);

        joiner = new StringJoiner("，");

        for (int datum : array) {
            joiner.add(String.valueOf(datum));
        }

        System.out.printf("排序后数组数据为: %s%n", joiner);
    }

    /**
     * 生成随机大小数组
     *
     * @param count 数组长度
     *
     * @return 返回一个指定长度随机生成的数组
     */
    public static int[] arrayGeneration(int count) {
        int[] res = new int[count];
        Random random = new Random();
        for (int i = 0; i < count; i++) {
            res[i] = random.nextInt(1000);
        }

        return res;
    }

    /**
     * 归并排序具体实现
     * 将这个数组一直对半拆分，直到拆分成每个元素为一个数组，然后在向上对比合并
     * leetcode 912算法题 执行用时:23ms, 超过85.87%，内存消耗：50.5mb，超过92.11%
     *
     * @param nums 待排序数组
     *
     * @return 排序后数组
     */
    public int[] mergeSorting(int[] nums) {
        int[] target = new int[nums.length];

        this.sort(nums, target, 0, nums.length - 1);

        return nums;
    }

    private void sort(int[] src, int[] target, int start, int end) {
        if (start >= end) {
            return;
        }

        // 数组拆分
        int length = end - start, middle = (length >> 1) + start;

        int leftStart = start, leftEnd = middle;
        int rightStart = middle + 1, rightEnd = end;

        this.sort(src, target, leftStart, leftEnd);
        this.sort(src, target, rightStart, rightEnd);

        int index = start;

        // 数组合并
        while (leftStart <= leftEnd && rightStart <= rightEnd) {
            target[index++] = src[leftStart] < src[rightStart] ? src[leftStart++] : src[rightStart++];
        }

        while (leftStart <= leftEnd) {
            target[index++] = src[leftStart++];
        }

        while (rightStart <= rightEnd) {
            target[index++] = src[rightStart++];
        }

        if (end + 1 - start >= 0) {
            System.arraycopy(target, start, src, start, end + 1 - start);
        }
    }

    /**
     * 归并排序 循环版
     * 来自：https://zhuanlan.zhihu.com/p/124356219
     * @param arr
     */
    public static void merge_sort(int[] arr) {
        int len = arr.length;
        int[] result = new int[len];
        int block, start;

        // 原版代码的迭代次数少了一次，没有考虑到奇数列数组的情况
        for (block = 1; block < len * 2; block *= 2) {
            for (start = 0; start < len; start += 2 * block) {
                int low = start;
                int mid = Math.min((start + block), len);
                int high = Math.min((start + 2 * block), len);
                // 两个块的起始下标及结束下标
                int start1 = low, end1 = mid;
                int start2 = mid, end2 = high;
                // 开始对两个block进行归并排序
                while (start1 < end1 && start2 < end2) {
                    result[low++] = arr[start1] < arr[start2] ? arr[start1++] : arr[start2++];
                }
                while (start1 < end1) {
                    result[low++] = arr[start1++];
                }
                while (start2 < end2) {
                    result[low++] = arr[start2++];
                }
            }
            int[] temp = arr;
            arr = result;
            result = temp;
        }
        result = arr;
    }
}

```

## 复杂度和稳定性
### 复杂度
> 平均时间复杂度：O(nlog<sub>2</sub>n)  
> 最佳时间复杂度：O(n)  
> 最差时间复杂度：O(nlog<sub>2</sub>n)  
> 空间复杂度：O(n)


不管元素在什么情况下都要做这些步骤，所以花销的时间是不变的，所以该算法的最优时间复杂度和最差时间复杂度及平均时间复杂度都是一样的为：O(nlog<sub>2</sub>n)  
归并的空间复杂度就是那个临时的数组和递归时压入栈的数据占用的空间：**n + log<sub>2</sub>n**；所以空间复杂度为: O(n)。  
### 稳定性
> 稳定性：稳定  

归并排序算法中，归并最后到底都是相邻元素之间的比较交换，并不会发生相同元素的相对位置发生变化，故是稳定性算法。

