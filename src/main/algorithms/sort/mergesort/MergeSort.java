package main.algorithms.sort.mergesort;

import java.util.Random;
import java.util.StringJoiner;

/**
 * 归并排序具体实现
 *
 * @author jingzepei
 * @version 1.0
 * @since 2022/12/24 15:27
 */
public class MergeSort {
    public static void main(String[] args) {
        int[] array = arrayGeneration(10);

        StringJoiner joiner = new StringJoiner("，");

        for (int datum : array) {
            joiner.add(String.valueOf(datum));
        }

        System.out.printf("原始数组数据为: %s%n", joiner);


        MergeSort mergeSort = new MergeSort();
        mergeSort.mergeSorting(array);

        joiner = new StringJoiner("，");

        for (int datum : array) {
            joiner.add(String.valueOf(datum));
        }

        System.out.printf("排序后数组数据为: %s%n", joiner);
    }

    /**
     * 生成随机大小数组
     *
     * @param count 数组长度
     *
     * @return 返回一个指定长度随机生成的数组
     */
    public static int[] arrayGeneration(int count) {
        int[] res = new int[count];
        Random random = new Random();
        for (int i = 0; i < count; i++) {
            res[i] = random.nextInt(1000);
        }

        return res;
    }


    /**
     * 归并排序具体实现
     * 将这个数组一直对半拆分，直到拆分成每个元素为一个数组，然后在向上对比合并
     * leetcode 912算法题 执行用时:23ms, 超过85.87%，内存消耗：50.5mb，超过92.11%
     *
     * @param nums 待排序数组
     *
     * @return 排序后数组
     */
    public int[] mergeSorting(int[] nums) {
        int[] target = new int[nums.length];

        this.sort(nums, target, 0, nums.length - 1);

        return nums;
    }

    private void sort(int[] src, int[] target, int start, int end) {
        if (start >= end) {
            return;
        }

        // 数组拆分
        int length = end - start, middle = (length >> 1) + start;

        int leftStart = start, leftEnd = middle;
        int rightStart = middle + 1, rightEnd = end;

        this.sort(src, target, leftStart, leftEnd);
        this.sort(src, target, rightStart, rightEnd);

        int index = start;

        // 数组合并
        while (leftStart <= leftEnd && rightStart <= rightEnd) {
            target[index++] = src[leftStart] < src[rightStart] ? src[leftStart++] : src[rightStart++];
        }

        while (leftStart <= leftEnd) {
            target[index++] = src[leftStart++];
        }

        while (rightStart <= rightEnd) {
            target[index++] = src[rightStart++];
        }

        if (end + 1 - start >= 0) {
            System.arraycopy(target, start, src, start, end + 1 - start);
        }
    }
}
