# 排序算法----Shell排序
编写时间：2022-12-12 21:22

## 概念
Shell排序是直接插入排序的改进版，基本思想就是：对于n个待排序的元素，**取一个小于n(一般是直接n / 2)的整数作为步长**将待排序的元素平均分为， 
若干份，**每一份中的元素与相邻元素下标之差都是步长。然后，分别对每一份进行直接插入排序**。这一趟下来，每一份中的元素都是有序的；然后缩小步长，重复
上述步骤。重复这样的操作，**当且仅当步长为1时，整个数组就是有序的**。

## Shell排序实现
下面以数列``{9, 1, 2, 5, 7, 4, 8, 6, 3, 5}``为例，演示它的希尔排序过程。
![img.png](img.png)

- 操作步骤：
    - 初始时，有一个大小为 10 的无序序列。 
      - 在第一趟排序中，我们不妨设 gap1 = N / 2 = 5，即相隔距离为 5 的元素组成一组，可以分为 5 组。 
      - 接下来，按照直接插入排序的方法对每个组进行排序。 
    - 在第二趟排序中，我们把上次的 gap 缩小一半，即 gap2 = gap1 / 2 = 2 (取整数)。这样每相隔距离为 2 的元素组成一组，可以分为 2 组。 
      - 按照直接插入排序的方法对每个组进行排序。 
      - 在第三趟排序中，再次把 gap 缩小一半，即gap3 = gap2 / 2 = 1。 这样相隔距离为 1 的元素组成一组，即只有一组。 
      - 按照直接插入排序的方法对每个组进行排序。此时，排序已经结束。

### 代码实现
```java
public class ShellSort {

    public static void main(String[] args) {
        ShellSort sort = new ShellSort();
        int[] src = new int[] {5, 2, 3, 1};
        StringJoiner joiner = new StringJoiner("，");

        for (int datum : src) {
            joiner.add(String.valueOf(datum));
        }

        System.out.printf("原始数组数据为: %s%n", joiner);

        // 递归实现
        // src = sort.shellSort(src, src.length / 2);
        
        // 遍历实现
        src = sort.shellSort(src);
        
        joiner = new StringJoiner("，");

        for (int datum : src) {
            joiner.add(String.valueOf(datum));
        }

        System.out.printf("排序后数组数据为: %s%n", joiner);
    }
    
    /**
     * 循环调用法
     * @param nums
     * @return
     */
    public int[] shellSort(int[] nums) {
        for (int gap = nums.length / 2; gap > 0; gap /= 2) {
            for (int i = 0; i < gap; i++) {
                for (int j = i + gap; j < nums.length; j += gap) {
                    if (nums[j] < nums[j - gap]) {
                        int temp = nums[j], middle = j - gap;
                        for (; middle >=0 && temp < nums[middle]; middle -= gap) {
                            nums[middle + gap] = nums[middle];
                        }

                        nums[middle + gap] = temp;
                    }
                }
            }
        }
        
        return nums;
    }

    /**
     * 递归实现
     * @param nums  待排序的数组
     * @param gap   步长
     * @return
     */
    public int[] shellSort(int[] nums, int gap) {
        if (gap < 1) {
            return nums;
        }

        for (int i = 0; i < gap; i++) {
            for (int j = i + gap; j < nums.length; j += gap) {
                if (nums[j] < nums[j - gap]) {
                    int temp = nums[j], middle = j - gap;
                    for (; middle >= 0 && temp < nums[middle]; middle -= gap) {
                        nums[middle + gap] = nums[middle];
                    }

                    nums[middle + gap] = temp;
                }
            }
        }

        return this.shellSort(nums, gap / 2);
    }
}
```

## 效率分析
## 算法性能
![img_1.png](img_1.png)
### 时间复杂度
**Shell排序的时间复杂度与步长有关**，如果步长为1，则直接退化成直接插入排序，这时候时间复杂度为O(n<sup>2</sup>)，而Hibbard增量的希尔排序
的时间复杂度为O(N<sup>1.5</sup>)
### 空间复杂度
由直接插入排序算法可知，我们在排序过程中，需要一个临时变量存储要插入的值，所以空间复杂度为O(1)。
### 稳定性
希尔排序中相等数据可能会交换位置，所以希尔排序是不稳定的算法。


