package main.algorithms.sort.shellsort;

import java.util.Random;
import java.util.StringJoiner;

/**
 * 排序算法----希尔排序
 *
 * @author jingzepei
 * @version 1.0
 * @since 2022/12/12 21:00
 */
public class ShellSort {
    public static void main(String[] args) {
        ShellSort sort = new ShellSort();
        // int[] src = arrayGeneration(5);
        int[] src = new int[] {5, 2, 3, 1};
        StringJoiner joiner = new StringJoiner("，");

        for (int datum : src) {
            joiner.add(String.valueOf(datum));
        }

        System.out.printf("原始数组数据为: %s%n", joiner);

        sort.shellSort(src, src.length / 2);

        joiner = new StringJoiner("，");

        for (int datum : src) {
            joiner.add(String.valueOf(datum));
        }

        System.out.printf("排序后数组数据为: %s%n", joiner);
    }

    /**
     * 生成随机大小数组
     *
     * @param count 数组长度
     *
     * @return 返回一个指定长度随机生成的数组
     */
    public static int[] arrayGeneration(int count) {
        int[] res = new int[count];
        Random random = new Random();
        for (int i = 0; i < count; i++) {
            res[i] = random.nextInt();
        }

        return res;
    }

    /**
     * 希尔排序具体实现，希尔排序其实就是直接插入排序的优化版，将整个数组分块进行插入排序，直到最后的步长为1，在整体排序
     * 在leetcode 912 执行时间为：24ms, 在所有java提交中击败了81.16%
     *
     * @param src 待排序的数组
     */
    public void shellSort(int[] src) {
        for (int gap = src.length / 2; gap > 0; gap /= 2) {
            for (int i = 0; i < gap; i++) {
                for (int j = i + gap; j < src.length; j += gap) {
                    if (src[j] < src[j - gap]) {
                        int temp = src[j], middle = j - gap;
                        for (; middle >= 0 && temp < src[middle]; middle -= gap) {
                            src[middle + gap] = src[middle];
                        }

                        src[middle + gap] = temp;
                    }
                }
            }
        }
    }

    /**
     * 递归实现
     * @param nums  待排序的数组
     * @param gap   步长
     * @return
     */
    public int[] shellSort(int[] nums, int gap) {
        if (gap < 1) {
            return nums;
        }

        for (int i = 0; i < gap; i++) {
            for (int j = i + gap; j < nums.length; j += gap) {
                if (nums[j] < nums[j - gap]) {
                    int temp = nums[j], middle = j - gap;
                    for (; middle >= 0 && temp < nums[middle]; middle -= gap) {
                        nums[middle + gap] = nums[middle];
                    }

                    nums[middle + gap] = temp;
                }
            }
        }

        return this.shellSort(nums, gap / 2);
    }
}
