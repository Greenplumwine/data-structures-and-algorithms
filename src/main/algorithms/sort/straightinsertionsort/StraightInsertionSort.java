package main.algorithms.sort.straightinsertionsort;

import java.util.Random;
import java.util.StringJoiner;

/**
 * 插入排序实现逻辑
 *
 * @author jingzepei
 * @version 1.0
 * @since 2022/12/12 12:58
 */
public class StraightInsertionSort {
    public static void main(String[] args) {
        StraightInsertionSort sort = new StraightInsertionSort();
        int[] src = arrayGeneration(5);

        StringJoiner joiner = new StringJoiner("，");

        for (int datum : src) {
            joiner.add(String.valueOf(datum));
        }

        System.out.printf("原始数组数据为: %s%n", joiner);

        sort.straightInsertionSorting(src);

        joiner = new StringJoiner("，");

        for (int datum : src) {
            joiner.add(String.valueOf(datum));
        }

        System.out.printf("排序后数组数据为: %s%n", joiner);
    }

    /**
     * 生成随机大小数组
     *
     * @param count 数组长度
     *
     * @return 返回一个指定长度随机生成的数组
     */
    public static int[] arrayGeneration(int count) {
        int[] res = new int[count];
        Random random = new Random();
        for (int i = 0; i < count; i++) {
            res[i] = random.nextInt();
        }

        return res;
    }

    /**
     * 插入排序实现
     * <p>在leetcode 912 题上时间超时</p>
     * <p>时间复杂度：O(n<sup>2</sup>); 空间复杂度为O(1); 属于稳定算法</p>
     * @param src 待排序的数组
     */
    public void straightInsertionSorting(int[] src) {
        // 根据直接插入排序基本思想，将第一个数作为有序表，其他元素作为无需表
        for (int start = 1; start < src.length; start++) {
            // 比较相邻两个数的大小，当前的数小于前一个数的时候
            if (src[start] < src[start - 1]) {
                // 记录一下当前的数
                int temp = src[start];
                // 从前一个数向前遍历，把每一个大于当前数的元素下标向后移动一位
                int end = start - 1;
                for (; end >= 0 && temp < src[end]; end--) {
                    src[end + 1] = src[end];
                }
                // 然后将当前数插入即可
                src[end + 1] = temp;
            }
        }
    }
}
