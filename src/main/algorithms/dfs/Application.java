package main.algorithms.dfs;

import java.util.ArrayList;
import java.util.List;

/**
 * 深度优先遍历Leet Code
 *
 * @author jingzepei
 * @version 1.0
 * @since 2023/2/21 21:15
 */
public class Application {
    public static void main(String[] args) {
        // 字典序排序
        List<Integer> res = Application.lexicalOrder(13);
        System.out.println(res);
    }


    /**
     * <p>题目：字典序排数</p>
     * <ul>
     *     <li>来源：Leet Code </li>
     *     <li>题号：386</li>
     *     <li>难度: <span style="color:orange">中等</span></li>
     *     <li>
     *         给你一个整数 n ，按字典序返回范围 [1, n] 内所有整数。你必须设计一个时间复杂度为 O(n) 且使用 O(1) 额外空间的算法。
     *     </li>
     *     <li>
     *         <ul>
     *             <li>
     *                 <p>示例1：</p>
     *                 <p>
     *                     <p>输入： n = 13</p>
     *                     <p>输出： [1,10,11,12,13,2,3,4,5,6,7,8,9]</p>
     *                 </p>
     *             </li>
     *             <li>
     *                 <p>示例2：</p>
     *                 <p>
     *                     <p>输入： n = 2</p>
     *                     <p>输出： [1,2]</p>
     *                 </p>
     *             </li>
     *         </ul>
     *     </li>
     *     <li>
     *         <p>提示：</p>
     *         <ul>
     * 	          <li>1 <= n <= 5 * 10<sup>4</sup></li>
     *          </ul>
     *     </li>
     * </ul>
     *
     * @param n 结尾值
     */
    public static List<Integer> lexicalOrder(int n) {
        // 把逻辑想成遍历10叉树
        List<Integer> res = new ArrayList<>();
        int num = 1;
        for (int i = 0; i < n; i++) {
            res.add(num);
            if (num * 10 < n) {
                num *= 10;
            } else {
                while (num % 10 == 9 || num + 1 > n) {
                    num /= 10;
                }
                num++;
            }
        }

        return res;
    }
}
