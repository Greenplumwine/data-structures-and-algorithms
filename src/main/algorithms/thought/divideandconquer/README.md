# 算法思想 ———— 分治算法
编写时间: 2023-01-02 14:04
## 概念
分治算法的基本思想：  
将一个规模为N的问题分解成K个较小规模的子问题，这些子问题相互独立且与原问题性质相同，将子问题的解求出，就可得到原问题的解
> 说人话：
> 将一个大问题，根据情况的分成多个小问题，将这些小问题的解求出，合并一下就能得到大问题的结果。

## 注意
分治算法只是一种算法的思想，需要根据不同的情况分成多个问题进行求解，难就难在，怎样将大问题拆分成多个小问题，然后将多个小问题的解合并

## 例题
1. [Leet Code 241 👉🏻 为运算表达式设计优先级](https://leetcode.cn/problems/different-ways-to-add-parentheses/)
```java
/**
 * 算法思想---分治算法
 *
 * @author jingzepei
 * @version 1.0
 * @since 2023/1/3 13:11
 */
public class TheIdeaOfDivideAndConquer {
    public static void main(String[] args) {
        TheIdeaOfDivideAndConquer application = new TheIdeaOfDivideAndConquer();

        List<Integer> result = application.diffWaysToCompute("2-1-1");

        for (Integer integer : result) {
            System.out.print(integer + " ");
        }
    }

    /**
     * Leet Code 241 全局变量
     * 备忘录，用于排除已经计算过的表达式
     */
    public Map<String, List<Integer>> diffWaysToComputeMemorandum = new HashMap<>();

    /**
     * <p>题目：为运算表达式设计优先级</p>
     * <ul>
     *     <li>来源：Leet Code </li>
     *     <li>题号：241</li>
     *     <li>难度: <span style="color:orange">中等</span></li>
     *     <li>
     *         题目：给你一个由数字和运算符组成的字符串 expression ，按不同优先级组合数字和运算符，计算并返回所有可能组合的结果。你可以按任意顺序返回答案。
     *         生成的测试用例满足其对应输出值符合 32 位整数范围，不同结果的数量不超过10<sup>4</sup>
     *     </li>
     *     <li>
     *         <p>提示：</p>
     *         <ul>
     * 	            <li>1 <= expression.length <= 20</li>
     * 	            <li>expression 由数字和算符 '+'、'-' 和 '*' 组成。</li>
     * 	            <li>输入表达式中的所有整数值在范围 [0, 99]</li>
     *          </ul>
     *     </li>
     * </ul>
     *
     * @param expression 需要计算的运算表达式
     *
     * @return 所有可能组合的结果
     */
    public List<Integer> diffWaysToCompute(String expression) {

        if (diffWaysToComputeMemorandum.containsKey(expression)) {
            return diffWaysToComputeMemorandum.get(expression);
        }

        List<Integer> items = new ArrayList<>();

        for (int i = 0; i < expression.length(); i++) {
            char character = expression.charAt(i);

            if (character != '+' && character != '-' && character != '*') {
                continue;
            }

            List<Integer> left = this.diffWaysToCompute(expression.substring(0, i));
            List<Integer> right = this.diffWaysToCompute(expression.substring(i + 1));

            for (Integer leftValue : left) {
                for (Integer rightValue : right) {
                    switch (character) {
                        case '+':
                            items.add(leftValue + rightValue);
                            break;
                        case '-':
                            items.add(leftValue - rightValue);
                            break;
                        case '*':
                            items.add(leftValue * rightValue);
                            break;
                    }
                }
            }
        }

        if (items.isEmpty()) {
            items.add(Integer.valueOf(expression));
        }

        diffWaysToComputeMemorandum.put(expression, items);

        return items;
    }

}
```